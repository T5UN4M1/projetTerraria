#ifndef MOBILEENTITY_H
#define MOBILEENTITY_H

#include "Entity.h"
class MobileEntity : public Entity
{
public:
    /**
    *Constructeur d'une entit� mobile
    *\param x position x
    *\param y position y
    *\param speed sa vitesse
    *\param angle son angle qui permet de savoir sa direction
    */
    MobileEntity(float x,float y,float speed,float angle);
    /**
    *permet le d�placement d'une entit� mobile
    */
    void move();
    /**
    *accesseur en lecture de la vitesse
    *\return la vitesse
    */
    inline float getSpeed()const;
    /**
    *accesseur en lecture de l'angle
    *\return l'angle
    */
    inline float getAngle()const;

protected:
    float speed;
    float angle;

private:
};

float MobileEntity::getSpeed()const
{
    return speed;
}
float MobileEntity::getAngle()const
{
    return angle;
}
#endif // MOBILEENTITY_H
