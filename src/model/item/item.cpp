#include "item.h"

Item::Item()
{
    //ctor
}
Item::Item( ItemType type)
{
    this->type=type;
}
int Item::getIdFromType(ItemType type)
{
    int id;
    switch(type)
    {
    case ItemType::none:
        id=0;
        break;
    case ItemType::dirtBlock:
        id=1;
        break;
    case ItemType::stoneBlock:
        id=2;
        break;
    case ItemType::woodPlank:
        id=3;
        break;
    case ItemType::woodPlatform:
        id=4;
        break;
    case ItemType::copperPickaxe:
        id=5;
        break;
    case ItemType::copperAxe:
        id=6;
        break;
    case ItemType::copperHammer:
        id=7;
        break;
    case ItemType::copperSword:
        id=8;
        break;

    }
    return id;
}
ItemType Item::getType()
{
    return type;
}
int Item::getId()
{
    return getIdFromType(this->type);
}
int Item::getStackSize()
{
    int size=0;
    switch(this->type)
    {
    case ItemType::dirtBlock:
    case ItemType::stoneBlock:
    case ItemType::woodPlank:
    case ItemType::woodPlatform:
        size=999;
        break;
    case ItemType::copperAxe:
    case ItemType::copperHammer:
    case ItemType::copperPickaxe:
    case ItemType::copperSword:
        size=1;
        break;
    }
    return size;
}
Item::~Item()
{
}
vector <int> Item::getStats()
{
    vector <int> info(2);
    info[0]=getIdFromType(this->type);
    info[1]=getStackSize();
    return info;
}
