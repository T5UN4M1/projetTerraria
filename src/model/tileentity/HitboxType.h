#ifndef HITBOXTYPE_H
#define HITBOXTYPE_H

/**
*\enum HitboxType {  block,platform,none}
*/
enum class HitboxType
{
    block,
    platform,
    none
};

#endif // HITBOXTYPE_H
