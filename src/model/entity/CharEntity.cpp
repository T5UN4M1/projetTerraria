#include "CharEntity.h"

CharEntity::CharEntity(float x,float y,float speed,float speedMax,float angle,RectangleHitbox hitbox,SolidEntityType type):SolidEntity(x,y,speed,angle,hitbox,type)
{
    CharEntity::status=0;
    CharEntity::maxSpeed=speedMax;
}
int CharEntity::getStatut()
{
    return status;
}
float CharEntity::getMaxSpeed()const
{
    return maxSpeed;
}
void CharEntity::yield()
{
}
CharEntity::~CharEntity()
{
}
