#include "ProjetTerrariaCore.h"
#include "tileentity/WorldElements.h"
#include "tileentity/HitboxType.h"
#include "entity/enemy/Bat.h"
std::unique_ptr<ProjetTerrariaCore> ProjetTerrariaCore::instanceCore;


ProjetTerrariaCore::ProjetTerrariaCore(unsigned xSize,unsigned ySize,int* progression):frame(0),player(Player(0,0,0,0,0,RectangleHitbox(0,0)))
{
    Util::initRng();
    map.reset(xSize,ySize);
    for(int x=0;x<xSize;++x){
            *progression = static_cast<int>((x/static_cast<float>(xSize))* 100000.0);
        for(int y=0;y<ySize;++y){

            if(y<ySize/2){
                if(y<ySize/2 && y>ySize/2-3 && x%10 == 0){
                    map.set(x,y).reset(new TileEntity(WorldElements::stone,HitboxType::block));
                } else if(y == ySize/2-4) {
                    map.set(x,y).reset(new TileEntity(WorldElements::woodPlatform,HitboxType::platform));
                } else {
                    map.set(x,y).reset(new TileEntity(WorldElements::nothing,HitboxType::none));
                }
            } else if(y >= ySize/2 && y < ySize/2 + 5) {
                map.set(x,y).reset(new TileEntity(WorldElements::dirt,HitboxType::block));
            } else {
                map.set(x,y).reset(new TileEntity(WorldElements::stone,HitboxType::block));
            }
        }
    }

    // placement du joueur

    // d�finition de la taille des cases
    caseSize = 16;

    // d�finition de la taille du joueur
    int playerHeight = 44;
    int playerWidth = 28;

    //on d�finit les coordonn�es du joueur,
    // x = milieu de map adapt� en fonction de la largeur du joueur
    // y = tout en haut de la map, on descendra case par case jusqu'� ce que l'entit� ne soit plus plac�e de fa�on valide vis � vis des blocs de la map
    int xPlacement = (xSize * caseSize) / 2 - playerWidth / 2;
    int yPlacement = (caseSize-playerHeight%caseSize)%caseSize;

    // on cr�e player et l'entit� t, t servira � tester les positions jusqu'� ce qu'elle ne soit plus correcte , player ne prendra que des positions correctes
    SolidEntity t(xPlacement,0,0,0,RectangleHitbox(playerWidth,playerHeight),SolidEntityType::STANDING);
    player = Player(t.getMinX(),t.getMinY(),0,6,90,RectangleHitbox(playerWidth,playerHeight));

    // on d�finit free qui passe � false lorsque t est en collision avec un bloc, tant que la position des pieds du joueur est comprise dans la map, on fait descendre t � chaque passage de caseSize
    for(bool free = true;t.getHashedMaxY(caseSize) < map.getYSize() && free;t.movePos(0,caseSize)){
        // pour chaque case occup�e totalement ou partielement par t, on teste l'hitbox
        for(int x=t.getHashedMinX(caseSize);x<=t.getHashedMaxX(caseSize) && free;++x){
            for(int y=t.getHashedMinY(caseSize);y<=t.getHashedMaxY(caseSize);++y){
                if(map.get(x,y)->getHitbox() != HitboxType::none){
                    free = false;
                    break;
                }
            }
        }
        // si on a pas eu deMobileEntity:: probleme avec t alors on peut placer le joueur � cet endroit
        if(free){
            player.setPos(t.getMinX(),t.getMinY()-0.00001);
        }
    }
}
void ProjetTerrariaCore::yield(){
    if(!player.isDead()){
        player.sEntityMove();
        if(player.getMinX() < 0){
            player.setX(0);
        } else if(player.getMaxX() > map.getXSize() * caseSize){
            player.setX(map.getXSize() * caseSize - player.getWidth());
        }
        if(player.getMinY() < 0){
            player.setY(0);
        } else if(player.getMaxY() > map.getYSize() * caseSize){
            player.setY(map.getYSize() * caseSize - player.getHeight());
        }
    }
    // g�re tout ce qui se passe en 1 frame
    if(frame%600 == 10){
        enemies.push_back(std::unique_ptr<Enemy>(new Bat(player.getX() - 500,player.getY() - 50)));
    }

    for(std::list<std::unique_ptr<Enemy>>::iterator e = getEnemies().begin();e != getEnemies().end();){
        if((*e)->isDead()){
            e = enemies.erase(e);
        } else {
            (*e)->yield();
            (*e)->sEntityMove();
            if((*e)->collisionWith(player) && !player.isInvulnerable()){
                player.damage((*e)->getMeleeDamage());
                player.setInvulnerable(30);
            }
            ++e;
        }
    }
    ++frame;
}
Vector2D<std::unique_ptr<TileEntity>>& ProjetTerrariaCore::getMap(){
    return map;
}
void ProjetTerrariaCore::setInstance(ProjetTerrariaCore* instance){
    instanceCore.reset(instance);
}
std::unique_ptr<ProjetTerrariaCore>& ProjetTerrariaCore::instance(){
    return instanceCore;
}
bool ProjetTerrariaCore::areBlocksFree(int x,int y,int w,int h,std::vector<HitboxType> allowedBlocks)const{
    if(!isIntoMap(x,y,w,h)){
        return false;
    }
    for(int X=x;X< x+w;++X){
        for(int Y=y;Y< y+h;++Y){
            bool isOk = false;
            for(HitboxType& hitbox : allowedBlocks){
                if(map.get(X,Y)->getHitbox() == hitbox){
                    isOk = true;
                    break;
                }
            }
            if(!isOk){
                return false;
            }
        }
    }
    return true;
}
HitboxType ProjetTerrariaCore::getStrongestHitbox(int x,int y,int w,int h)const{
    // on cherche � savoir quel est le bloc le plus bloquant dans le carr� fourni, est ce que tout est de l'air, est ce qu'on a au moins une platforme ou un bloc ?
    if(!isIntoMap(x,y,w,h)){
        return HitboxType::block;
    }
    HitboxType hb = HitboxType::none;
    for(int X=x;X< x+w;++X){
        for(int Y=y;Y< y+h;++Y){
            switch(map.get(X,Y)->getHitbox()){
            case HitboxType::none:
                break;
            case HitboxType::platform:
                hb = HitboxType::platform;
                break;
            case HitboxType::block:
                return HitboxType::block;
            }
        }
    }
    return hb;
}
std::list<ItemEntity> ProjetTerrariaCore::getItemEntity(){
    return this->itementity;
}
bool ProjetTerrariaCore::isIntoMap(int x,int y)const{
    return x >= 0 && y >= 0 && x <= map.getXSize() && y <= map.getYSize();
}
bool ProjetTerrariaCore::isIntoMap(int x,int y,int w,int h)const{
    return isIntoMap(x,y) && isIntoMap(x+w,y+h);
}
