#include "TickingEntity.h"

TickingEntity::TickingEntity(SpecialEntityType type, int a_x, int a_y)
    :SpecialEntity(type), x(a_x), y(a_y)
{}

bool TickingEntity::isMultiBlock(){
    return true;
}

bool TickingEntity::isInteractable(){
    return true;
}

TickingEntity::~TickingEntity()
{}
