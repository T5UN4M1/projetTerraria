#include "SolidEntity.h"

#include "../tileentity/TileEntity.h"
#include "../tileentity/HitboxType.h"

#include "../ProjetTerrariaCore.h"


SolidEntity::SolidEntity(float x,float y,float speed,float angle,RectangleHitbox hitbox,SolidEntityType type):MobileEntity(x,y,speed,angle),hitbox(hitbox),type(type)
{

}

bool SolidEntity::isOnABlock()const{
    float difference = (getHashedMaxY(Core::instance()->getCaseSize()) * Core::instance()->getCaseSize()) - getMaxY();
    if(difference < 0.1){
        for(int i=getHashedMinX();i<=getHashedMaxX();++i){
            if(Core::instance()->getMap().get(i,getHashedMaxY()+1)->getHitbox() != HitboxType::none){
                return true;
            }
        }
    }
    return false;
}
void SolidEntity::sEntityMove(){
    bool falling = true;
    bool platformInteraction = true;
    switch(type){
        case SolidEntityType::FLYING_NO_COLLISION:
            move();
            return;
        case SolidEntityType::STANDING:
            break;
        case SolidEntityType::FLYING_NO_PHYSICS:
            falling = false;
            platformInteraction = false;
            break;
        case SolidEntityType::FLYING:
            platformInteraction = false;
            break;
    }
    // chang� en interpolation
    // on bouge l'entit� petit � petit jusqu'� avoir un probl�me ,on rollback et on adapte la pos

    int interp = ((speed / Core::instance()->getCaseSize())+1)*static_cast<int>(speed)*4; // une interp plus �lev�e �quivaut � des mouvements plus pr�cis mais un temps de calcul plus �lev�
    // on d�finit le vecteur de d�placement, cela permettra de connaitre le point qu'on va utiliser ensuite de fa�on plus fiable que d'utiliser l'angle
    float vx = Util::getXFromMove(speed,angle);
    float vy = Util::getYFromMove(speed,angle);

    if(!isOnABlock() && falling){
        vy += 0.5;
        speed = Util::getSpeedFromVector(vx,vy);
        angle = Util::getAngleFromVector(vx,vy);
    }
    HitboxType startHB = Core::instance()->getStrongestHitbox(
            getHashedMinX(Core::instance()->getCaseSize()),
            getHashedMinY(Core::instance()->getCaseSize()),
            (getHashedMaxX(Core::instance()->getCaseSize()) - getHashedMinX(Core::instance()->getCaseSize()))+1,
            (getHashedMaxY(Core::instance()->getCaseSize()) - getHashedMinY(Core::instance()->getCaseSize()))+1
        );
    if(startHB == HitboxType::block){ // au moins un des blocks "occup�s" par l'entit� est un block, l'entit� ne devrait meme pas se trouver la , elle ne peut pas bouger d'ici
        return;
    }
    if(speed == 0){
        return;
    }

    std::pair<Direction,Direction> movingDirection;
    movingDirection.first = (vx > 0) ? Direction::RIGHT : Direction::LEFT;
    movingDirection.second = (vy > 0) ? Direction::BOTTOM : Direction::TOP;
    if(vx == 0){
        movingDirection.first = Direction::NONE;
    }
    if(vy == 0){
        movingDirection.second = Direction::NONE;
    }
    float interpVx = vx/interp;
    float interpVy = vy/interp;
    for(int i=0;i<interp;++i){
        x+=interpVx;
        y+=interpVy;
        bool collision = false;

        switch(movingDirection.second){
        case Direction::TOP:{
            HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMinY(), (getHashedMaxX()-getHashedMinX()) + 1, 1);
            if(hb == HitboxType::block){// mouvement bloqu�
                x-=interpVx;
                y-=interpVy;

                speed = Util::getSpeedFromVector(vx,0);
                angle = Util::getAngleFromVector(vx,0);
                return;
            }
            break;
        }
        case Direction::BOTTOM:{
            HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMaxY(), (getHashedMaxX()-getHashedMinX()) + 1, 1);
            if(hb == HitboxType::block || (platformInteraction && startHB == HitboxType::none && hb == HitboxType::platform)){// mouvement bloqu�
                x-=interpVx;
                y-=interpVy;

                speed = Util::getSpeedFromVector(vx,0);
                angle = Util::getAngleFromVector(vx,0);
                return;
            }
            break;
        }
        default:
            break;
        }
        switch(movingDirection.first){
        case Direction::RIGHT:{
            // on test les carr�s de droite
            HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMaxX(), getHashedMinY(), 1, (getHashedMaxY()-getHashedMinY()) + 1);
            if(hb == HitboxType::block){// mouvement bloqu�
                x-=interpVx;
                y-=interpVy;

                speed = Util::getSpeedFromVector(0,vy);
                angle = Util::getAngleFromVector(0,vy);
                return;
            }
            break;
        }
        case Direction::LEFT:{
            HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMinY(), 1, (getHashedMaxY()-getHashedMinY()) + 1);
            if(hb == HitboxType::block){// mouvement bloqu�
                x-=interpVx;
                y-=interpVy;

                speed = Util::getSpeedFromVector(0,vy);
                angle = Util::getAngleFromVector(0,vy);
                return;
            }
            break;
        }
        default:
            break;
        }
    }




    /*
    // sert � bouger l'entit� en prenant en compte la map (collision avec map)

    if(Core::instance()->getStrongestHitbox(
            getHashedMinX(Core::instance()->getCaseSize()),
            getHashedMinY(Core::instance()->getCaseSize()),
            (getHashedMaxX(Core::instance()->getCaseSize()) - getHashedMinX(Core::instance()->getCaseSize()))+1,
            (getHashedMaxY(Core::instance()->getCaseSize()) - getHashedMinY(Core::instance()->getCaseSize()))+1
        ) == HitboxType::block){ // au moins un des blocks "occup�s" par l'entit� est un block, l'entit� ne devrait meme pas se trouver la , elle ne peut pas bouger d'ici
        return;
    }
    if(speed == 0){
        return;
    }
    // on d�finit le vecteur de d�placement, cela permettra de connaitre le point qu'on va utiliser ensuite de fa�on plus fiable que d'utiliser l'angle
    float vx = Util::getXFromMove(speed,angle);
    float vy = Util::getYFromMove(speed,angle);

    std::pair<Direction,Direction> movingDirection;
    movingDirection.first = (vx > 0) ? Direction::RIGHT : Direction::LEFT;
    movingDirection.second = (vy > 0) ? Direction::BOTTOM : Direction::TOP;



    //float oldX = x;
    //float oldY = y;

    // xy ratio du vecteur de mouvement
    float xyRatio = vx/(vy != 0.0 ? vy : 0.0000001);

    //x += vx;
    //y += vy;

    // le point d'arriv�e th�orique aka sans obstacle
    float destX = x + vx;
    float destY = y + vy;

    // cette boucle va faire avancer l'entit� de case en case horizontalement ou verticalement, lorsque la boucle se termine, l'entit� se trouve � son emplacement final
    // les valeurs x y prennent donc la nouvelle position et speed angle peuvent changer lorsqu'un obstacle est rencontr�
    while(true){

        float originX = (vx > 0) ? getMaxX() : getMinX();
        float originY = (vy > 0) ? getMaxY() : getMinY();

        // l'objectif est de d�terminer si on va dabord occuper les cases au dessus en bas a gauche ou a droite de l'aire occup�e � la base (hashed min max)
        // on calcule le point (1 des 4 points du carr� tilemap occup� par l'entit�, celui qui est le plus proche de la destination
        // l'objectif est de savoir si on va se d�ploacer verticalement ou horizontalement , pour cela on va comparer le ratio xy de (goal - origin)   avec le ratio xy du vecteur (xyRatio)
        float goalX = ((movingDirection.first == Direction::RIGHT) ? (getHashedMaxX() + 1) :  getHashedMinX()) * Core::instance()->getCaseSize();
        float goalY = ((movingDirection.second == Direction::BOTTOM) ? (getHashedMaxY() + 1) :  getHashedMinY()) * Core::instance()->getCaseSize();

        if(((movingDirection.first  == Direction::RIGHT  && goalX > destX)  ||
            (movingDirection.first  == Direction::LEFT   && goalX < destX)) &&
           ((movingDirection.second == Direction::BOTTOM && goalY > destY)  ||
            (movingDirection.second == Direction::TOP    && goalY < destY))){
            // si le point de destination th�orique est plus proche que le prochain "sommet" , �a veut dire qu'on a plus de test de collision � faire
            // on peut donc tout simplement bouger l'entit� � sa destination et arr�ter l�
            x = destX;
            y = destY;
            return;
        }
        float origin2GoalX = goalX - originX;
        float origin2GoalY = goalY - originY;

        float origin2GoalRatio = origin2GoalX/ (origin2GoalY != 0.0 ? origin2GoalY : 0.0000001);

        // il faut maintenant d�terminer dans quelle direction on va chercher

        Direction dir = Direction::NONE;
        if(vy == 0){
            dir = movingDirection.first;
        } else if(vx == 0){
            dir = movingDirection.second;
        } else {
            // on prend les carr�s pour d�gager les ratios n�gatifs , si le ratio xy du mouvement est inf�rieur  au ratio sommet->goal alors le d�placement est vertical on prend donc le second
            dir = (xyRatio*xyRatio < origin2GoalRatio*origin2GoalRatio) ? movingDirection.second : movingDirection.first;
        }
        // on a d�fini la direction dans laquelle on va se d�placer, il ne reste plus qu'� op�rer le d�placement de fa�on � rentrer � peine dans le nouveau carr� de d�limitation
        Direction stuckBy = Direction::NONE;
        switch(dir){
            case Direction::LEFT:{
                HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX()-1, getHashedMinY(), 1, (getHashedMaxY()-getHashedMinY()) + 1);
                if(hb != HitboxType::block){ // les cases sont libres -> on bouge
                    int mMinY = getHashedMinY();
                    int mMaxY = getHashedMaxY();
                    float target = (getHashedMinX() * Core::instance()->getCaseSize()) - 0.00001;
                    y = Util::getMoveToTarget(x,y,vx,vy,target,true);
                    x = target;
                    if(getHashedMinY() != mMinY){ // on s'est l�gerement trop d�plac� vers le haut (s�curit�, arrivera tr�s peu, mais pas impossible, � cause du -0.00001)
                        return;//y = mMinY * Core::instance()->getCaseSize() + 0.000001; // on force l'entit� � reprendre un y acceptable et on laisse comme �a, le prochain passage de boucle remettra l'entit� dans sa bonne position
                    } else if(getHashedMaxY() != mMaxY){
                        return;//y =
                        // actions � d�finir / v�rifier
                    }
                } else { // le mouvement est bloqu�
                    float target = (getHashedMinX() * Core::instance()->getCaseSize()) + 0.00001;
                    y = Util::getMoveToTarget(x,y,vx,vy,target,true);
                    x = target;
                    stuckBy = Direction::LEFT;
                }
                break;
            }
            case Direction::TOP:{
                HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMinY()-1, (getHashedMaxX()-getHashedMinX()) + 1,1);
                if(hb != HitboxType::block){ // les cases sont libres -> on bouge
                    int mMinX = getHashedMinX();
                    int mMaxX = getHashedMaxX();
                    float target = (getHashedMinY() * Core::instance()->getCaseSize()) - 0.00001;
                    x = Util::getMoveToTarget(x,y,vx,vy,target,false);
                    y = target;
                    if(getHashedMinX() != mMinX){ // on s'est l�gerement trop d�plac� vers le haut (s�curit�, arrivera tr�s peu, mais pas impossible, � cause du -0.00001)
                        //y = mMinY * Core::instance()->getCaseSize() + 0.000001; // on force l'entit� � reprendre un y acceptable et on laisse comme �a, le prochain passage de boucle remettra l'entit� dans sa bonne position
                    } else if(getHashedMaxX() != mMaxX){
                        //y =
                        // actions � d�finir / v�rifier
                    }
                } else { // le mouvement est bloqu�
                    float target = (getHashedMinY() * Core::instance()->getCaseSize()) + 0.00001;
                    x = Util::getMoveToTarget(x,y,vx,vy,target,false);
                    y = target;
                    stuckBy = Direction::TOP;
                }
                break;
            }
            case Direction::RIGHT:{
                HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMaxX()+1, getHashedMinY(), 1, (getHashedMaxY()-getHashedMinY()) + 1);
                if(hb != HitboxType::block){ // les cases sont libres -> on bouge
                    int mMinY = getHashedMinY();
                    int mMaxY = getHashedMaxY();
                    float target = (((getHashedMaxX()+1) * Core::instance()->getCaseSize()) + 0.00001) - getWidth();
                    y = Util::getMoveToTarget(x,y,vx,vy,target,true);
                    x = target;
                    if(getHashedMinY() != mMinY){ // on s'est l�gerement trop d�plac� vers le haut (s�curit�, arrivera tr�s peu, mais pas impossible, � cause du -0.00001)
                        //y = mMinY * Core::instance()->getCaseSize() + 0.000001; // on force l'entit� � reprendre un y acceptable et on laisse comme �a, le prochain passage de boucle remettra l'entit� dans sa bonne position
                    } else if(getHashedMaxY() != mMaxY){
                        //y =
                        // actions � d�finir / v�rifier
                    }
                } else { // le mouvement est bloqu�
                    float target = (((getHashedMaxX()+1) * Core::instance()->getCaseSize()) - 0.00001) - getWidth();
                    y = Util::getMoveToTarget(x,y,vx,vy,target,true);
                    x = target;
                    stuckBy = Direction::RIGHT;
                }
                break;
            }
            default:
            case Direction::BOTTOM:{
                HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMaxY()+1, (getHashedMaxX()-getHashedMinX()) + 1,1);
                if(hb == HitboxType::none){ // les cases sont libres -> on bouge attention car la condition d'hitbox change par rapport aux autres car les paltformes n'autorisent pas le mouvement de haut en bas
                    int mMinX = getHashedMinX();
                    int mMaxX = getHashedMaxX();
                    float target = (((getHashedMaxY()+1) * Core::instance()->getCaseSize()) + 0.00001) - getHeight();
                    x = Util::getMoveToTarget(x,y,vx,vy,target,false);
                    y = target;
                    if(getHashedMinX() != mMinX){ // on s'est l�gerement trop d�plac� vers le haut (s�curit�, arrivera tr�s peu, mais pas impossible, � cause du -0.00001)
                        //y = mMinY * Core::instance()->getCaseSize() + 0.000001; // on force l'entit� � reprendre un y acceptable et on laisse comme �a, le prochain passage de boucle remettra l'entit� dans sa bonne position
                    } else if(getHashedMaxX() != mMaxX){
                        //y =
                        // actions � d�finir / v�rifier
                    }
                } else { // le mouvement est bloqu�
                    float target = (((getHashedMaxY()+1) * Core::instance()->getCaseSize()) - 0.00001) - getHeight();
                    x = Util::getMoveToTarget(x,y,vx,vy,target,false);
                    y = target;
                    stuckBy = Direction::BOTTOM;
                }
                break;
            }
        }
        if(stuckBy != Direction::NONE){ // le mouvement est bloqu� , la position est calcul�s mais on va devoir s'arreter ici et changer vitesse & angle
            switch(stuckBy){
            case Direction::RIGHT:case Direction::LEFT:
                vx = 0;
                break;
            default:
                vy = 0;
            }
            angle = Util::getSpeedFromVector(vx,vy);
            speed = Util::getAngleFromVector(vx,vy);
            return;
        }
    }*/


}


int SolidEntity::getHashedMinX()const{
    return static_cast<int>(getMinX() / Core::instance()->getCaseSize());
}

int SolidEntity::getHashedMinY()const{
    return static_cast<int>(getMinY() / Core::instance()->getCaseSize());
}

int SolidEntity::getHashedMaxX()const{
    return static_cast<int>(getMaxX() / Core::instance()->getCaseSize());
}

int SolidEntity::getHashedMaxY()const{
    return static_cast<int>(getMaxY() / Core::instance()->getCaseSize());
}
