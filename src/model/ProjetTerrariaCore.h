#ifndef PROJETTERRARIACORE_H
#define PROJETTERRARIACORE_H

#include "model/entity/ItemEntity.h"
#include "tileentity/TileEntity.h"
#include "util/Vector2D.h"
#include "entity/Player.h"
#include "entity/enemy.h"
#include <list>


class ProjetTerrariaCore
{
    public:
        ProjetTerrariaCore(unsigned xSize,unsigned ySize,int* progression);
        void yield();
        Vector2D<std::unique_ptr<TileEntity>>& getMap();

        inline Player& getPlayer();

        inline int getCaseSize()const;
        inline std::list<ItemEntity> getItemEntity();


        inline void setPlayerMove(Direction direction);
        inline int getFrame();

        static void setInstance(ProjetTerrariaCore* instance);
        static std::unique_ptr<ProjetTerrariaCore>& instance();

        bool areBlocksFree(int x,int y,int w,int h,std::vector<HitboxType> allowedBlocks)const;
        HitboxType getStrongestHitbox(int x,int y,int w,int h)const;
        bool isIntoMap(int x,int y)const;
        bool isIntoMap(int x,int y,int w,int h)const;

        inline float getAngleToPlayer(float x,float y)const;

        inline std::list<std::unique_ptr<Enemy>>& getEnemies();

        //List<unique_ptr<TickingEntity>>

    protected:

    private:
        static std::unique_ptr<ProjetTerrariaCore> instanceCore;
        int frame;
        Vector2D<std::unique_ptr<TileEntity>> map;
       int caseSize;

        Player player;
        std::list<ItemEntity> itementity;

        std::list<std::unique_ptr<Enemy>> enemies;
        /*std::vector<std::vector<TileEntity>> map;
        int xSize;
        int ySize;*/
};
typedef ProjetTerrariaCore Core;

Player& Core::getPlayer(){
    return player;
}
int Core::getCaseSize()const{
    return caseSize;
}
void Core::setPlayerMove(Direction direction){
    player.movePlayer(direction);
}
int Core::getFrame(){
    return frame;
}
float Core::getAngleToPlayer(float x,float y)const{
    return Util::getAngleFromVector(player.getCenterX() -x ,player.getCenterY() - y);

}
std::list<std::unique_ptr<Enemy>>& Core::getEnemies(){
    return enemies;
}
#endif // PROJETTERRARIACORE_H
