#include "MenuItemImg.h"
#include "../ProjetTerrariaView.h"

MenuItemImg::MenuItemImg(sf::Sprite& img, sf::Sprite& imgF){
    this->img = sf::Sprite(img);
    this->imgF = sf::Sprite(imgF);
    initHitbox();
}

void MenuItemImg::yield(bool selected){
    ProjetTerrariaView::rt.draw(selected ? imgF : img);
}

void MenuItemImg::initHitbox(){
    this->hitbox = this->img.getGlobalBounds();
}
