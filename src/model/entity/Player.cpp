#include "Player.h"
#include "../tileentity/HitboxType.h"
#include "../ProjetTerrariaCore.h"
#include "../item/tool.h"
Player::Player(float x,float y,float speed,float maxSpeed,float angle,RectangleHitbox hitbox):CharEntity(x,y,speed,maxSpeed,angle,hitbox,SolidEntityType::STANDING),HealthPointsEntity(100),inventory(Inventory(9,5))
{
    inventory.add(0,0,new Tool(ItemType::copperPickaxe),1);
    inventory.add(1,0,new Tool(ItemType::copperAxe),1);
    inventory.add(2,0,new Tool(ItemType::copperHammer),1);
    inventory.add(3,0,new Tool(ItemType::copperSword),1);
}
Player::Player(float x,float y,float speed,float maxSpeed,float angle,RectangleHitbox hitbox,std::string name):Player(x,y,speed,maxSpeed,angle,hitbox)
{

}

Direction Player::getAngleOrientation()
{
    return angleOrientation;
}
void Player::movePlayer(Direction dir)
{
   float vx=Util::dCos(angle)*speed;
   float vy=Util::dSin(angle)*speed;
    switch(dir)
    {
    case Direction::RIGHT:
        angleOrientation=Direction::RIGHT;
        vx=vx + this->maxSpeed/30;
        if(vx > maxSpeed){
            vx = maxSpeed;
        }
        break;
    case Direction::LEFT:
        angleOrientation=Direction::LEFT;
        vx=vx - this->maxSpeed/30;
        if(vx < -maxSpeed){
            vx = -maxSpeed;
        }
        break;
    case Direction::TOP:
        if(isOnABlock()){
            vy = -10;
        }
        break;
    case Direction::BOTTOM:
        if(isOnABlock()){
            HitboxType hb = Core::instance()->getStrongestHitbox(getHashedMinX(), getHashedMaxY()+1, (getHashedMaxX()-getHashedMinX()) + 1, 1);
            if(hb == HitboxType::platform){
                ++y;
            }
        }

   default:
        if(vx > 0){
            vx=vx - this->maxSpeed/30;
            if(vx < 0){
                vx = 0;
            }
        }
        else if(vx < 0){
            vx=vx + this->maxSpeed/30;
            if(vx > 0){
                vx = 0;
            }
        }
        break;

    }
    this->speed=Util::getSpeedFromVector(vx,vy);
    this->angle=Util::getAngleFromVector(vx,vy);
    /*if(dir == Direction::LEFT){
        this->speed=Util::getSpeedFromVector(-5,0);
        this->angle=Util::getAngleFromVector(-5,0);
    }
    if(dir == Direction::RIGHT){
        this->speed=Util::getSpeedFromVector(5,0);
        this->angle=Util::getAngleFromVector(5,0);
    }*/
}

    void Player::yield()
    {
    }
    Player::~Player()
    {
//dtor
    }

