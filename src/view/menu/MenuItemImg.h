#ifndef MENUITEMIMG_H
#define MENUITEMIMG_H

#include <SFML/graphics.hpp>
#include "MenuItem.h"

class MenuItemImg : public MenuItem
{
    public:
        MenuItemImg(sf::Sprite& img,sf::Sprite& imgF);

        virtual void yield(bool selected);
        virtual void initHitbox();

    protected:
        sf::Sprite img;
        sf::Sprite imgF;
    private:
};

#endif // MENUITEMIMG_H
