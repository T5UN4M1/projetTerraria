#ifndef CHUNK_H
#define CHUNK_H
#include "../Media.h"
#include "../../model/util/Vector2D.h"
#include "../../model/tileentity/TileEntity.h"
#include <SFML/graphics.hpp>

class Chunk : public sf::Drawable, public sf::Transformable
{
    public:
        Chunk();
        Chunk(sf::Vector2i size,sf::Vector2i start);

        void reload(Vector2D<std::unique_ptr<TileEntity>>& map);

    protected:
        sf::Vector2i start; // coordonn�e x y en nombre de blocs � laquelle cette chunk demarre
        sf::Vector2i size; // taille en blocs de la chunk
        sf::VertexArray tiles; // conteneur des vertexs

        void updateVertex(int x,int y,sf::Vertex* v);
    private:

    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const{
        // on applique la transformation de l'entit� -- on la combine avec celle qui a �t� pass�e par l'appelant
        states.transform *= getTransform(); // getTransform() est d�finie par sf::Transformable
        // on applique la texture
        states.texture = &Media::txt["tiles"];
        // on dessine le tableau de vertex
        target.draw(tiles, states);
    }
};

#endif // CHUNK_H
