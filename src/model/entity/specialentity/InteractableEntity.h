#ifndef __INTERCTABLEENTITY__H__
#define __INTERCTABLEENTITY__H__
#include "SpecialEntity.h"

class InteractableEntity: public SpecialEntity
{

public:

    InteractableEntity(SpecialEntityType type);

    virtual bool isMultiBlock();

    virtual bool isInteractable();

    virtual ~InteractableEntity();

    //OnRightClick()
    //OnLeftClick();

};

#endif
