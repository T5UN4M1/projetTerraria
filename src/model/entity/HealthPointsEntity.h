#ifndef HEALTHPOINTSENTITY_H
#define HEALTHPOINTSENTITY_H


class HealthPointsEntity
{
    public:
        HealthPointsEntity(int maxHp);
        HealthPointsEntity(int hp,int maxHp);
        int getHp()const;
        int getMaxHp()const;

        //inflige des d�gats a l'entit� , diminue hp selont damage ,
        //ne peut pas tomber en n�gatif
        void damage(int damage);

        //permet de savoir si on est mort (hp == 0)
        bool isDead()const;

        bool isInvulnerable()const;
        void setInvulnerable(int frames);
        int getDeathFrame()const;

        void heal(int hp);
        void heal();
    protected:
        int hp;
        int maxHp;

        int invulnerableUntil;
        int deathFrame;
};
#endif // ENTITY_H
