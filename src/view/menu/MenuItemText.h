#ifndef MENUITEMTEXT_H
#define MENUITEMTEXT_H

#include <SFML/graphics.hpp>
#include "MenuItem.h"

class MenuItemText : public MenuItem
{
    public:
        MenuItemText(sf::Text& text,sf::Text& textF);

        virtual void yield(bool selected);
        virtual void initHitbox();

    protected:
        sf::Text text;
        sf::Text textF;
    private:
};

#endif // MENUITEMTEXT_H
