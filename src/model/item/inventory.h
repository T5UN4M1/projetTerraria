#ifndef INVENTORY_H
#define INVENTORY_H
#include "model/item/inventory.h"
#include "model/util/Vector2D.h"
#include "model/item/Item.h"
#include "model/item/slot.h"

class Inventory
{
public:
    /**
    *Constructeur par d�fault de l'inventaire
    */
    Inventory();
    /**
    * Constructeur de l'inventaire avec une taille x et une taille y du vecteur 2D
    *\param x nombre de ligne du vecteur 2D
    *\param y nombre de colonne du vecteur 2D
    */
    Inventory(int x,int y);
    /**
    *destructeur par d�fault
    */

    virtual ~Inventory();
    /**
    *Ajout d'un slot a la position x y dans le Vecteur 2D
    *\param x num�ro de ligne
    *\param y num�ro de colonne
    *\param item l'item
    *\param amount nombre d'occurence de cette item
    */
    void add(int x,int y,Item *item,int amount);
    /**
    * rajoute les items donn�s aux slots. Priorit� aux slots qui sont du m�me type s'il y en a
    *\param item l'item
    *\param amount nombre d'occurence de cette item
     */
    void add(Item *item, int amount);
    /**
    * compte le nombre de slots vide dans le vecteur 2D
    *\return le nombre de slots vides
    */
    int getRoomLeft();
    /**
    * retourne les slots en brut
    *\return les slots
    */
    inline Vector2D<Slot>& getVector();

protected:
    Vector2D<Slot> slots;

private:
};

Vector2D<Slot>& Inventory::getVector(){
    return slots;
}
#endif // INVENTORY_H
