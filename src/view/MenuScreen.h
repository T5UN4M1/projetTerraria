#ifndef MENUSCREEN_H
#define MENUSCREEN_H

#include "menu/menu.h"
#include "controllerstates/MenuStates.h"
#include "input/Input.h"

class MenuScreen
{
    public:
        static Menu menu;

        static void yield();
        static MenuStates state;
        static int frame;

        static std::unique_ptr<Input> controls;
        static std::vector<sf::Text> texts;
        static std::vector<sf::Sprite> sprites;

        static void setState(MenuStates state);
};

#endif // MENUSCREEN_H
