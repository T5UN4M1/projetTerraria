#ifndef BUTTON_H
#define BUTTON_H

#include "InputAction.h"

class Button : public InputAction
{
    public:
        Button();

        virtual bool isPushed()const;
        virtual bool isActivated(int delay,int interval) const;

        virtual int getFrames()const;

        virtual void yield();
    protected:
        int framePushed;

};

#endif // BUTTON_H
