#include "MultiBlockEntity.h"

MultiBlockEntity::MultiBlockEntity(SpecialEntityType type)
    :SpecialEntity(type)
{}

bool MultiBlockEntity::isMultiBlock(){
    return true;
}

bool MultiBlockEntity::isInteractable(){
    return true;
}

MultiBlockEntity::~MultiBlockEntity()
{}
