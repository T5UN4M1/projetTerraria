#ifndef UTIL_H
#define UTIL_H

#define PI 3.14159265
#define RAD  180.0/PI   // ~ 57.295 , correspond au nombre de degr�s dans un radian
#define RAD2 PI/180.0
#define PY 0.70710678

#include <cmath>
#include <string>
#include <sstream>

#include <iostream>
#include <cstdlib>
#include <ctime>

class Util
{
    public:
    static double toDegree(double angle){
        return angle*RAD;
    }
    static double toRad(double angle){
        return angle*RAD2;
    }
    static double dSin(double angle){
        return sin(toRad(angle));
    }
    static double dCos(double angle){
        return cos(toRad(angle));
    }
    static float getXFromMove(float speed,float angle){
        return Util::dCos(angle)*speed;
    }
    static float getYFromMove(float speed,float angle){
        return Util::dSin(angle)*speed;
    }
    static float getSpeedFromVector(float x,float y){
        return sqrt((x*x)+(y*y));
    }
    static float getAngleFromVector(float x,float y){
        return toDegree(atan2(y,x));
    }
    template<class T>
    static std::string toStr(T nb){
        std::stringstream ss;
        ss << nb;
        return ss.str();
    }
    // permet de d�placer une entit� x y vers    target y2 ou x2 target (selon que isX soit true ou false) en calculant le mouvement via vx et vy
    // si isX est true alors on retourne y2, sinon on retourne x2
    static float getMoveToTarget(float x,float y,float vx,float vy,float target,bool isX){
        //float mov = 0;
        if(vx == 0 || vy == 0){ // si un des 2 vaut 0 le calcul est ou bien impossible (div par 0) ou la valeur ne changera pas ( * 0)
                // id�alement il faut traiter les cas vx == 0 et vy == 0 en dehors de cette fonction , cette condition sert � �viter de crash le programme
            return 0;
        }
        if(isX){// on cherche y2
            return x + ((target - x) / vx) * vy;
        } else {
            return y + ((target - y) / vy) * vx;
        }
    }
    static void initRng(){
        srand(time(NULL));
    }
    static int rng(int min,int max){
        return rand() % (max - min) + min;
    }
};

#endif // UTIL_H
