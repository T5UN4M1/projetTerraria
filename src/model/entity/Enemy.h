#ifndef ENEMY_H
#define ENEMY_H

#include "CharEntity.h"
#include "HealthPointsEntity.h"
#include "enemy/EnemyType.h"

class Enemy : public CharEntity , public HealthPointsEntity
{
    public:
        /**
        *Constructeur pour cr�er un enemie
        *\param x position x de l'�nemie
        *\param y position y de l'enemie
        *\param speed sa vitesse
        *\param maxSpeed sa vitesse maximum
        *\param angle son angle qui permet de savoir sa direction
        *\param hitbox rectangle pour les collisions
        *\param meleeDamage les d�gats de l'ennemi au CaC
        *\param type le type de physique de l'ennemi
        *\param hp le nbre d'hp et hp max de l'ennemi
        *\param enemyType le type d'ennemi
        */
        Enemy(float x,float y,float speed,float maxSpeed,float angle,RectangleHitbox hitbox,int meleeDamage,SolidEntityType type,int hp,EnemyType enemyType);
        /**
        * destructeur de l'objet
        */
        virtual ~Enemy();
        /**
        *calcul du rendement de l'�nemie
        */
        virtual void yield();



        inline EnemyType getEnemyType()const;

        inline int getMeleeDamage()const;
    protected:
        int meleeDamage;
        EnemyType enemyType;
    private:
};
EnemyType Enemy::getEnemyType()const{
    return enemyType;
}
int Enemy::getMeleeDamage()const{
    return meleeDamage;
}
#endif // ENEMY_H
