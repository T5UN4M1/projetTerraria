#include "HealthPointsEntity.h"
#include "../ProjetTerrariaCore.h"
HealthPointsEntity::HealthPointsEntity(int maxHp)
    :hp(maxHp),maxHp(maxHp)
{invulnerableUntil = 0;}

HealthPointsEntity::HealthPointsEntity(int a_hp,int a_maxHp)
    :hp(a_maxHp),maxHp(a_maxHp)
{invulnerableUntil = 0;}

int HealthPointsEntity::getHp() const{
    return hp;
}

int HealthPointsEntity::getMaxHp() const{
    return maxHp;
}

void HealthPointsEntity::damage(int damage){
    if(isInvulnerable()){
        return;
    }
    hp = hp - damage;

    if(hp<0){
        hp=0;
    }
    if(hp == 0){
        deathFrame = Core::instance()->getFrame();
    }
}

bool HealthPointsEntity::isDead()const{
 return hp==0;
}

bool HealthPointsEntity::isInvulnerable()const{
    return Core::instance()->getFrame() < invulnerableUntil;
}
void HealthPointsEntity::setInvulnerable(int frames){
    invulnerableUntil = Core::instance()->getFrame() + frames;
}
int HealthPointsEntity::getDeathFrame()const{
    return Core::instance()->getFrame() - deathFrame;
}
void HealthPointsEntity::heal(int hp){
    this->hp += hp;
    if(hp>maxHp){
        heal();
    }
}
void HealthPointsEntity::heal(){
    hp = maxHp;
}
