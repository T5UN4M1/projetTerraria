#ifndef RECTANGLEHITBOX_H
#define RECTANGLEHITBOX_H

#include "Hitbox.h"

class RectangleHitbox: public Hitbox
{
    public:
        /**
        * Constructeur d'un rectangle avec une certaine largeur et longueur
        *\param width la largeur du rectangle
        *\param height la hauteur du rectangle
        */
        RectangleHitbox(float width,float height);
        /**
        *accesseur en lecture de la largeur du rectangle
        *\return la largeur du rectangle
        */
        inline float getWidth() const;
         /**
        *accesseur en lecture de la hauteur du rectangle
        *\return la longueur du rectangle
        */
        inline float getHeight() const;
         /**
        *accesseur en �criture de la largeur du rectangle
        *\param w la nouvelle largeur du rectangle
        */
        inline void setWidth(float w);
        /**
        *accesseur en �criture de la longueur du rectangle
        *\param h la nouvelle longueur du rectangle
        */
        inline void setHeight(float h);

    protected:
        float width;
        float height;
    private:
};

float RectangleHitbox::getWidth() const{
    return this->width;
}

float RectangleHitbox::getHeight() const{
    return  this->height;
}

void RectangleHitbox::setWidth(float w){
    this->width = w;
}

void RectangleHitbox::setHeight(float h){
    this->height = h;
}

#endif // RECTANGLEHITBOX_H
