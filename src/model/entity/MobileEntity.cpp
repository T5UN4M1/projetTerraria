#include "MobileEntity.h"
#include "../util/Util.h"

MobileEntity::MobileEntity(float x,float y,float speed,float angle): Entity(x,y),speed(speed),angle(angle)
{

}
void MobileEntity::move(){
    movePos(Util::getXFromMove(speed,angle),Util::getYFromMove(speed,angle));
}
