#ifndef SPECIALENTITYTYPE_H
#define SPECIALENTITYTYPE_H
#include <iostream>
using namespace std;

enum class SpecialEntityType
{
    FurnaceEntity,
    DoorEntity
};

ostream & operator << (ostream &out, SpecialEntityType in);

#endif // SPECIALENTITYTYPE_H
