#ifndef SLOT_H
#define SLOT_H
#include <string>
#include <memory>
#include "model/item/Item.h"
#include "model/item/ItemType.h"



class Slot
{
public:
    /**
    *Constructeur par default qui met itemtype � none et amount a 0
    */
    Slot();
    /**
    *destructeur d'un slot
    */
    virtual ~Slot();
    /**
    *accesseur en lecture de l'item
    *\return pointeur d'un item
    */
    Item* getItem();
    /**
    *accesseur en lecture de amount
    *\return amount qui est le nombre d'occurence de l'item
    */
    int getAmount();
    /**
    * permet de valider item type et amount
    *\param type le type d'item
    *\param amount nombre d'occurence de item
    *\return vrai si l'item correspond et s'il ne d�passe pas le stacksize
    */
    bool canAccept(ItemType type,int amount);
    /**
    * verifie si un slot est vide
    *\return vrai si le slot est vide c'est � dire si itemtype est �gal a none ou que amount est �gal a 0
    */
    bool isEmpty()const;
    /**
    *ajout d'un item et amount a un slot s'il est vide
    *\param item un item
    *\param amount le nombre d'occurence de l'item
    */
    void add(Item *item,int amount);
    /**
    *permet de savoir combien d'item on peut encore ajouter en fonction du type d'items du stackSize
    *et de la quantit� actuellement dans le slot
    *\return nombre d'item qu'on peux ajouter
    */
    int getRoomLeft();
protected:
    Item * item;
    int amount;
private:
};

#endif // SLOT_H
