#include "input.h"

Input::Input(){
}

void Input::reset(){
    keySet.clear();
}

void Input::yield(){
    for(int i=0;i<keySet.size();++i){
        keySet[i]->yield();
    }
}

void Input::addButton(InputAction* button){
    keySet.push_back(std::unique_ptr<InputAction>(button));
}

bool Input::isPressed(int id)const{
    return keySet[id%keySet.size()]->isPushed();
}

bool Input::isActivated(int id, int delay, int interval)const{
    return keySet[id%keySet.size()]->isActivated(delay,interval);
}
