#ifndef CHARENTITY_H
#define CHARENTITY_H

#include "SolidEntity.h"

class CharEntity  : public SolidEntity
{
public:
    /**
    *Constructeur qui permet de cr�er une entit�
    *\param x position x
    *\param y position y
    *\param speed la vitesse de d�placement
    *\param angle l'angle de d�placement
    *\param type le type de physique pour l'entit�
    */
    CharEntity(float x,float y,float speed,float speedMax,float angle,RectangleHitbox hitbox,SolidEntityType type);
    /**
    * Destructeur
    */
    virtual ~CharEntity();
    /**
    * Calcul du Rendement
    */
    virtual void yield();
    /**
    * Fonction qui renvoie le status sous forme d'entier
    *\return un entier qui correspond a l'�tat de l'entit�
    */
    int getStatut();
    /**
    *Accesseur en lecture de la vitesse maximum
    *\return la vitesse maximum de l'entit�
    */
    float getMaxSpeed() const;
protected:
    int status;
    float maxSpeed;
private:
};

#endif // CHARENTITY_H
