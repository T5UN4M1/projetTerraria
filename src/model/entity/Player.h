#ifndef PLAYER_H
#define PLAYER_H

#include "CharEntity.h"
#include "../util/Direction.h"
#include "../util/Util.h"

#include "../item/Inventory.h"
#include "HealthPointsEntity.h"

class Player : public CharEntity , public HealthPointsEntity
{
public:
    Player(float x,float y,float speed,float maxSpeed,float angle,RectangleHitbox hitbox);
    Player(float x,float y,float speed,float maxSpeed,float angle,RectangleHitbox hitbox,std::string name);
    virtual ~Player();

    void movePlayer(Direction dir);
    Direction getAngleOrientation();

    void setAngleOrientation(std::pair<Direction,Direction> orientation);
    virtual void yield();

    inline Inventory& getInventory();

    inline int getSelect()const;
    inline int setSelect(int s);
protected:
    Direction angleOrientation;
    Inventory inventory;
    int selected;
private:

};
Inventory& Player::getInventory(){
    return inventory;
}
int Player::getSelect()const{
    return selected;
}
int Player::setSelect(int s){
    if(s >= inventory.getVector().getXSize() ){
        selected = 0;
    } else if(s < 0){
        this->selected = inventory.getVector().getXSize() - 1;
    } else {
        selected = s;
    }
}
#endif // PLAYER_H
