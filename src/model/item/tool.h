#ifndef TOOL_H
#define TOOL_H
#include "model/item/item.h"
#include "model/item/itemtype.h"


class Tool:public Item
{
public:
    /**
    *Constrtucteur par default ne fais rien de sp�cial
    */
    Tool();
    /**
    *Constructeur d'un tool a l'aide d'un type d'item passer en param�tre qui permet de d�terminer les variables d'un tool
    *\param type un itemtype
    */
    Tool(ItemType type);
    /**
    *Destructeur d'un tool
    */
    virtual ~Tool();
    /**
    * remplie un vecteur d'entier avec 14 entr�es. Chaque entr�e contiens une caract�ristique du tool
    *\return un vecteur d'entier
    */
    vector<int> getStats();


protected:

private:
    bool isPickaxe;
    bool isAxe;
    bool isHammer;
    bool autoSwing;
    int minDamage;
    int maxDamage;
    int useTime;
    int ressourceDamage;
    int criticalChance;
    int criticalMultiplier;
    int knockback;
    int range;

};

#endif // TOOL_H
