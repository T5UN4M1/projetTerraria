#ifndef WORLDELEMENTS_H
#define WORLDELEMENTS_H

/**
*\enum WorldElement les �lements du monde terraria stone,dirt,woodPlatform,treeTrunk,nothing
*/
enum class WorldElements
{
    stone,
    dirt,
    woodPlatform,



    furnace,
    treeSapling,
    torch,
    treeTrunk,
    ironOre,
    door1,
    door2,
    door3,
    door11,
    door12,
    door13,
    door21,
    door22,
    door23,
    coalOre,
    branch1,
    branch2,
    woodPlank,
    treeTop11,
    treeTop12,
    treeTop13,
    treeTop14,
    treeTop15,
    treeTop21,
    treeTop22,
    treeTop23,
    treeTop24,
    treeTop25,
    treeTop31,
    treeTop32,
    treeTop33,
    treeTop34,
    treeTop35,
    treeTop41,
    treeTop42,
    treeTop43,
    treeTop44,
    treeTop45,
    treeTop51,
    treeTop52,
    treeTop53,
    treeTop54,
    treeTop55,

    nothing
};

#endif // WORLDELEMENTS_H
