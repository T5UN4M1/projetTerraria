#ifndef __SPECIALENTITY__H__
#define __SPECIALENTITY__H__
#include "SpecialEntityType.h"

class SpecialEntity
{

protected:
    SpecialEntityType a_type;

public:

    SpecialEntity(SpecialEntityType type);

    virtual bool isMultiBlock() = 0;

    virtual bool isInteractable() = 0;

    //virtual vector<ItemEntity*> delete();

    virtual ~SpecialEntity();

};

#endif
