#ifndef SOLIDENTITYTYPE_H
#define SOLIDENTITYTYPE_H


enum class SolidEntityType
{
    STANDING, // personnage qui collisionne et qui est affect� par la gravit�
    FLYING, // volant, affect� par la gravit� mais pas par les platformes
    FLYING_NO_PHYSICS, // volant , non affect� par la gravit� (trajectoire droite par ex)
    FLYING_NO_COLLISION // volant, non affect� par gravit� ,peut passer a travers les blocks de la map
};

#endif // SOLIDENTITYTYPE_H
