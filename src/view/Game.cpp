#include "Game.h"
#include "ProjetTerrariaView.h"
#include "Media.h"

#include "LoadingScreen.h"

#include "../model/tileentity/WorldElements.h"
#include <SFML/graphics.hpp>
#include "../model/util/Util.h"

int Game::mapSize = 0;
int Game::frame = 0;

int Game::chunkSize = 32;

Vector2D<Chunk> Game::chunks;
ProjetTerrariaCore* Game::game;


void Game::yield(){



    if(frame == 0){
        setupGamePrototype(1000,1000);
    }

    unsigned startY = 480/32; // pour affichage statique de test

    // on cree un rectangle dont les coordonn�es correspondent au scope du MODELE
    sf::IntRect screen = sf::IntRect(game->getPlayer().getCenterX()-View::window->getSize().x/2,
                                            game->getPlayer().getCenterY()-View::window->getSize().y/2,
                                            View::window->getSize().x,
                                            View::window->getSize().y
                                            );
    // on cree une entit� pour avoir acces aux m�thodes de spatial hashing
    SolidEntity t(screen.left,screen.top,0,0,RectangleHitbox(screen.width,screen.height),SolidEntityType::STANDING);

    // on g�n�re les offset pour savoir quels chunks on va dessiner
    sf::Vector2i topLeftOffset = sf::Vector2i(t.getHashedMinX(16*chunkSize),t.getHashedMinY(16*chunkSize));
    sf::Vector2i bottomRightOffset = sf::Vector2i(t.getHashedMaxX(16*chunkSize),t.getHashedMaxY(16*chunkSize));

    // on doit maintenant calculer la coordonn�e haut gauche � laquelle on va devoir commencer � dessiner les chunks

    // coordonn�e mod�le du chunk haut gauche
    sf::Vector2f firstChunkCoord = sf::Vector2f(topLeftOffset.x * 16 * chunkSize,topLeftOffset.y * 16 * chunkSize);

    // on compare cette coordonn�e avec le point haut gauche de screen pour connaitre le d�calage, donc le point auquel on va devoir dessiner la premiere chunk dans la vue
    sf::Vector2f firstChunkViewCoord = sf::Vector2f(firstChunkCoord.x - screen.left,firstChunkCoord.y - screen.top);

    for(int x=topLeftOffset.x;x<=bottomRightOffset.x;++x){
        for(int y=topLeftOffset.y;y<=bottomRightOffset.y;++y){
            if(x>=0 && y>=0 && x <= chunks.getXSize() && y <= chunks.getYSize()){
                chunks.set(x,y).setPosition(firstChunkViewCoord.x + (x-topLeftOffset.x)*16*chunkSize,firstChunkViewCoord.y + (y-topLeftOffset.y)*16*chunkSize);
                View::rt.draw(chunks.get(x,y));
            }
        }
    }
    sf::Text testing;
    testing.setFont(Media::font[0]);
    testing.setPosition(200,200);
    testing.setCharacterSize(30);
    testing.setString("to draw coord : " + Util::toStr(firstChunkViewCoord.x) + " :: " + Util::toStr(firstChunkViewCoord.y));
    View::rt.draw(testing);

    testing.setPosition(200,250);
    testing.setString("Vector 5:0 " + Util::toStr(Util::getAngleFromVector(5,0)) + " :: Vector -5,0 " + Util::toStr(Util::getAngleFromVector(-5,0)));
    View::rt.draw(testing);

    testing.setPosition(200,300);
    testing.setString("Player angle :" + Util::toStr(game->getPlayer().getAngle()));
    View::rt.draw(testing);

    testing.setPosition(200,350);
    testing.setString("Player pos :" + Util::toStr(game->getPlayer().getMinX()) + "::" + Util::toStr(game->getPlayer().getMinY()));
    View::rt.draw(testing);


    testing.setPosition(200,400);
    testing.setString("Vector angle 180 :" + Util::toStr(Util::dCos(180)) + "::" + Util::toStr(Util::dSin(180)));
    View::rt.draw(testing);


    testing.setPosition(200,450);
    testing.setString("SCREEN LEFTTOP :" + Util::toStr(screen.left) + "::" + Util::toStr(screen.top));
    View::rt.draw(testing);

    testing.setPosition(20,20);
    testing.setString("Pv : " + Util::toStr(game->getPlayer().getHp()) + "/" + Util::toStr(game->getPlayer().getMaxHp()));
    View::rt.draw(testing);



    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)){
        //game->getPlayer().movePos(-5,0);
        game->setPlayerMove(Direction::LEFT);
    }
    else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right)){
        game->setPlayerMove(Direction::RIGHT);
        //game->getPlayer().movePos(5,0);
    } else {
        game->setPlayerMove(Direction::NONE);
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up)){
        //game->getPlayer().movePos(-5,0);
        game->setPlayerMove(Direction::TOP);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down)){
        //game->getPlayer().movePos(-5,0);
        game->setPlayerMove(Direction::BOTTOM);
    }
    game->yield();

    // AFFICHAGE JOUEUR
    if(!game->getPlayer().isDead()){
        int sp = 0;

        if(game->getPlayer().getAngleOrientation() == Direction::RIGHT){
            sp +=3;
        }
        if(game->getPlayer().getSpeed() > 0.1){
            sp += (frame/6)%3;
        }

        Media::spPlayer[sp].setPosition(View::window->getSize().x/2,View::window->getSize().y/2);

        sf::Vector2f spSize(17,23);

        if(sp > 2){
            Media::spPlayer[sp].setScale((game->getPlayer().getWidth()/spSize.x)*-1,game->getPlayer().getHeight()/spSize.y); // �a servait a rien de faire 6 sprites en fait ><
        } else {
            Media::spPlayer[sp].setScale((game->getPlayer().getWidth()/spSize.x),game->getPlayer().getHeight()/spSize.y);
        }
        View::rt.draw(Media::spPlayer[sp]);
    }
    //affichage enemies
    for(std::list<std::unique_ptr<Enemy>>::iterator e = game->getEnemies().begin();e != game->getEnemies().end();++e){
        Media::other["enemyBat"].setPosition(
                                             Game::translatePosition((*e)->getX(),screen.left),
                                             Game::translatePosition((*e)->getY(),screen.top)
        );
        View::rt.draw(Media::other["enemyBat"]);
    }


    // gestion du select

    int selected = game->getPlayer().getSelect();
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Add)){
        game->getPlayer().setSelect(++selected);
    } else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract)){
        game->getPlayer().setSelect(--selected);
    }
    selected = game->getPlayer().getSelect();
    // affichage de l'inventaire joueur (petit)

    for(int x = 0;x<game->getPlayer().getInventory().getVector().getXSize();++x){

        int posX = 50+x*60;
        int posY = 50;
        sf::Sprite sp;
        if(x == selected){
            sp = Media::other["inventorySlot2"];
        } else {
            sp = Media::other["inventorySlot"];
        }
        sp.setPosition(posX,posY);
        View::rt.draw(sp);
        if(!game->getPlayer().getInventory().getVector().get(x,0).isEmpty()){
            switch(game->getPlayer().getInventory().getVector().set(x,0).getItem()->getType()){
                case ItemType::copperAxe:
                    Media::itemIcons["axe"].setPosition(posX+9,posY+9);
                    View::rt.draw(Media::itemIcons["axe"]);
                    break;
                case ItemType::copperPickaxe:
                    Media::itemIcons["pickaxe"].setPosition(posX+9,posY+9);
                    View::rt.draw(Media::itemIcons["pickaxe"]);
                    break;
                case ItemType::copperHammer:
                    Media::itemIcons["hammer"].setPosition(posX+9,posY+9);
                    View::rt.draw(Media::itemIcons["hammer"]);
                    break;
                case ItemType::copperSword:
                    Media::itemIcons["sword"].setPosition(posX+9,posY+9);
                    View::rt.draw(Media::itemIcons["sword"]);
                    break;
            }
        }
    }
    ++frame;
}

void Game::setupGamePrototype(int xSize,int ySize){
    //game = ProjetTerrariaCore(xSize,ySize);
    Core::setInstance(new ProjetTerrariaCore(xSize,ySize,&LoadingScreen::gameProgression));
    Game::game = Core::instance().get();
    unsigned chunkSize = 32;

    sf::Vector2i chunkAmount = sf::Vector2i(xSize/chunkSize - 1,ySize/chunkSize - 1);

    chunks.reset(chunkAmount.x,chunkAmount.y);

    for(int x=0;x< chunkAmount.x;++x){
        for(int y=0;y< chunkAmount.y;++y){
            chunks.set(x,y,Chunk(sf::Vector2i(chunkSize,chunkSize),sf::Vector2i(x*chunkSize,y*chunkSize)));
            chunks.set(x,y).reload(game->getMap());
            //chunks.get(x,y).
        }
    }
}
float Game::translatePosition(float modelPos,float screenPos){
    return modelPos-screenPos;
}
