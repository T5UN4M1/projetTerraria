#include "SpecialEntityType.h"


ostream & operator<< (ostream &out, SpecialEntityType in){

    switch (in)
    {
    case SpecialEntityType::FurnaceEntity:
        out << "FurnaceEntity";
        break;
    case SpecialEntityType::DoorEntity:
        out << "DoorEntity";
        break;
    default:
        //out << "Ce que je ne veux pas afficher !";
        break;
    }

    return out;

}

