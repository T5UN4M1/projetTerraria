#ifndef MEDIA_H
#define MEDIA_H

#include <unordered_map>
#include <memory>
#include <SFML/Graphics.hpp>

class Media
{
    public:

        static std::unordered_map<std::string,sf::Texture> txt;

        static bool loading;

        static sf::Sprite spLoadingScreenBg;
        static sf::Sprite spLoadingScreenBar;
        static sf::Sprite spLoadingScreenGauge;

        static std::unordered_map<std::string,sf::Sprite> itemIcons;
        static std::unordered_map<std::string,sf::Sprite> tools;

        static std::unordered_map<std::string,sf::Sprite> other;

        static sf::Sprite spMenuBg;

        static std::vector<sf::Sprite> spPlayer;




        static std::vector<sf::Font> font;


        static std::unique_ptr<sf::Thread> t;

        static void loadMinimal();
        static void loadForMenu();
        static void loadForGame();


        static void load(void (*function)());
};

#endif // MEDIA_H
