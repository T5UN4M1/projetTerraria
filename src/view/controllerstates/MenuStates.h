#ifndef MENUSTATES_H
#define MENUSTATES_H

enum class MenuStates
{
    FIRST_MAIN,
    MAIN,
    MAP_SIZE_SELECTION,
    CREDITS

};

#endif // MENUSTATES_H
