#ifndef PROJETTERRARIAVIEW_H
#define PROJETTERRARIAVIEW_H
#include <SFML/Graphics.hpp>
#include "../model/ProjetTerrariaCore.h"
#include "controllerstates/ViewStates.h"
#include <memory>

class ProjetTerrariaView
{
    public:
        static int frame;
        static ViewStates state;

        static void run();
        static void yield();
        static void setState(ViewStates state);


        static std::unique_ptr<ProjetTerrariaCore> game;

        static sf::RenderTexture rt;
        static std::unique_ptr<sf::RenderWindow> window;
    private:
};
typedef ProjetTerrariaView View;

#endif // PROJETTERRARIAVIEW_H
