#ifndef ITEMTYPE_H
#define ITEMTYPE_H

/**
*\enum enum itemtype
*{none , dirtBlock , stoneBlock , woodPlank , woodPlatform ,
* copperPickaxe,copperAxe ,copperHammer , copperSword}
*/
enum class ItemType
{
    none, dirtBlock, stoneBlock, woodPlank, woodPlatform, copperPickaxe,copperAxe,copperHammer, copperSword
};

#endif // ITEMTYPE_H
