#ifndef DIRECTION_H
#define DIRECTION_H

/**
*@enum LEFT,TOP,RIGHT,BOTTOM,NONE
*/

enum class Direction
{
    LEFT,TOP,RIGHT,BOTTOM,NONE
};

#endif // DIRECTION_H
