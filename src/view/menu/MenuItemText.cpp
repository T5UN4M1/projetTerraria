#include "MenuItemText.h"
#include "../ProjetTerrariaView.h"

MenuItemText::MenuItemText(sf::Text& text,sf::Text& textF){
    this->text = sf::Text(text);
    this->textF = sf::Text(textF);
    initHitbox();
}

void MenuItemText::yield(bool selected){
    ProjetTerrariaView::rt.draw(selected ? textF : text);
}

void MenuItemText::initHitbox(){
    this->hitbox = this->text.getGlobalBounds();
}
