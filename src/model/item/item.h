#ifndef ITEM_H
#define ITEM_H
#include "model/item/ItemType.h"
#include <vector>

using namespace std;
class Item
{
public:
    /**
    * Constructeur par d�fault ne faisrien de sp�cial
    */
    Item();
    /**
    * Constructeur d'un type d'item
    */
    Item (ItemType type);
    /**
    *Destructeur ar default
    */
    virtual ~Item();
    /**
    * Permet de convertir un itemtype en un nombre
    *\param type le type d'item a convertir sous forme de nombre
    *\return l'id qui correspond a l'itemtype
    */
    static int getIdFromType(ItemType type);
    /**
    *Convertie un nombre en un type d'item
    *\param type l'id qui correspond a un type d'item
    *\return un itemtype
    */
    ItemType getTypeFromId(int type);
    /**
    *accesseur en lecture de l'id de l'item
    *\return id du itemtype
    */
    int getId();
    /**
    *accesseur en lecture de itemtype
    *\return l'itemtype
    */
    ItemType getType();
    /**
    * calcule le nombre de fois que cette itemtype peu �tre rajout�
    * \return nombre de fois que cette itemtype peu �tre present dans le jeu
    */
    int getStackSize();
    /**
    * remplis un vecteur avec l'id et le stacksize de l'itemtype
    *\return un vecteur d'entier avec deux entr�es
    */
    virtual vector<int> getStats();



protected:
    ItemType type;
private:


};

#endif // ITEM_H
