#include "MenuScreen.h"
#include "Media.h"
#include "ProjetTerrariaView.h"
#include "menu/MenuItemText.h"

#include "input/Key.h"
#include "input/MouseButton.h"
#include "input/InputGroup.h"

Menu MenuScreen::menu;

MenuStates MenuScreen::state = MenuStates::FIRST_MAIN;
int MenuScreen::frame = 0;

std::unique_ptr<Input> MenuScreen::controls;
std::vector<sf::Text> MenuScreen::texts;
std::vector<sf::Sprite> MenuScreen::sprites;


void MenuScreen::yield(){
    // �x�cut� uniquement lorsque le menu apparait pour la premiere fois
    if(frame == 0 && state == MenuStates::FIRST_MAIN){
        // on cr�e un set de cointrole pour le menu
        controls.reset(new Input());
        std::vector<InputAction*> actions;
        actions.push_back(new Key(sf::Keyboard::Left));
        actions.push_back(new Key(sf::Keyboard::Up));
        controls->addButton(new InputGroup(actions));
        actions.clear();
        actions.push_back(new Key(sf::Keyboard::Right));
        actions.push_back(new Key(sf::Keyboard::Down));
        controls->addButton(new InputGroup(actions));
        actions.clear();
        actions.push_back(new Key(sf::Keyboard::Return));
        actions.push_back(new Key(sf::Keyboard::W));
        actions.push_back(new Key(sf::Keyboard::A));
        actions.push_back(new MouseButton(sf::Mouse::Left));
        controls->addButton(new InputGroup(actions));
        actions.clear();
        actions.push_back(new Key(sf::Keyboard::Escape));
        actions.push_back(new Key(sf::Keyboard::X));
        actions.push_back(new Key(sf::Keyboard::BackSpace));
        actions.push_back(new MouseButton(sf::Mouse::Right));
        controls->addButton(new InputGroup(actions));
        actions.clear();

        /*
        controls->addButton(new Key(sf::Keyboard::Left));
        controls->addButton(new Key(sf::Keyboard::Up));
        controls->addButton(new Key(sf::Keyboard::Right));
        controls->addButton(new Key(sf::Keyboard::Down));

        controls->addButton(new Key(sf::Keyboard::Return));
        controls->addButton(new Key(sf::Keyboard::W));
        controls->addButton(new Key(sf::Keyboard::A));
        controls->addButton(new MouseButton(sf::Mouse::Left));

        controls->addButton(new Key(sf::Keyboard::Escape));
        controls->addButton(new Key(sf::Keyboard::X));
        controls->addButton(new Key(sf::Keyboard::BackSpace));
        controls->addButton(new MouseButton(sf::Mouse::Right));*/

        // on entre maintenant vraiment dans le menu
        setState(MenuStates::MAIN);
    }

    View::rt.draw(Media::spMenuBg);

    // fonctions sp�ciales du menu
    switch(state){
    case MenuStates::MAIN: case MenuStates::MAP_SIZE_SELECTION:
        menu.yield();
        for(sf::Text& t : texts){
            View::rt.draw(t);
        }
        for(sf::Sprite& sp : sprites){
            View::rt.draw(sp);
        }
        break;
    }
    // gestion des controles
    controls->yield();
    bool down = controls->isActivated(1);//false;
    bool up = controls->isActivated(0);
    bool act = controls->isActivated(2);
    bool ret = controls->isActivated(3);
    // on fait en sorte de savoir ce que l'utilisateur veut faire en fonction des touches sur elsquelles il appuie
    /*for(int i=0;i<2;++i){
        if(controls->isActivated(i)){
            up = true;
            break;
        }
    }
    for(int i=2;i<4;++i){
        if(controls->isActivated(i)){
            down = true;
            break;
        }
    }
    for(int i=4;i<8;++i){
        if(controls->isActivated(i)){
            act = true;
            break;
        }
    }
    for(int i=8;i<12;++i){
        if(controls->isActivated(i)){
            ret = true;
            break;
        }
    }*/

    if(up){
        --menu;
    }else if(down){
        ++menu;
    }

    if(act){
        // actions d�finies lorsque le choix est confirm�
        switch(state){
        case MenuStates::MAIN :
            switch(menu.getSelect()){
            case 0: // play
                setState(MenuStates::MAP_SIZE_SELECTION);
                break;
            case 3: // exit
                View::window->close();
            }
            break;
        case MenuStates::MAP_SIZE_SELECTION :
            switch(menu.getSelect()){
            case 0:case 1:case 2:case 3:case 4:
                View::setState(ViewStates::LOADING_FOR_GAME);
            }
        }
    } else if(ret){
        // actions d�finies lorsque le menu est annul� (retour en arriere)
        switch(state){
        case MenuStates::MAIN :
            if(menu.getSelect() == 3){
                View::window->close();
            }
            menu.setSelect(3);
            break;
        case MenuStates::MAP_SIZE_SELECTION :
            setState(MenuStates::MAIN);
            break;
        }
    }
    ++frame;
}
void MenuScreen::setState(MenuStates state){
    // on vide le menu actuel
    menu.reset();
    sprites.clear();
    texts.clear();
    // on cree les texes avec options par d�faut pour le menu
    sf::Text default1;
    default1.setFont(Media::font[0]);
    default1.setCharacterSize(24);
    default1.setColor(sf::Color::White);

    sf::Text default2(default1);
    default2.setColor(sf::Color::Red);
    // on cr�e les �l�ments du menu
    std::vector<std::pair<sf::Vector2f,std::string>> el;
    switch(state){
        case MenuStates::MAIN :
            el.push_back(std::make_pair(sf::Vector2f(600,300),"Play"));
            el.push_back(std::make_pair(sf::Vector2f(610,340),"Options"));
            el.push_back(std::make_pair(sf::Vector2f(620,380),"Credits"));
            el.push_back(std::make_pair(sf::Vector2f(630,420),"Exit"));
            break;
        case MenuStates::MAP_SIZE_SELECTION :
            texts.push_back(sf::Text(default1));
            texts[0].setCharacterSize(36);
            texts[0].setString("Choisissez la taille de la carte que vous voulez jouer");
            texts[0].setPosition(550,200);
            el.push_back(std::make_pair(sf::Vector2f(600,300),"Tr�s Petite"));
            el.push_back(std::make_pair(sf::Vector2f(610,340),"Petite"));
            el.push_back(std::make_pair(sf::Vector2f(620,380),"Moyenne"));
            el.push_back(std::make_pair(sf::Vector2f(630,420),"Grande"));
            el.push_back(std::make_pair(sf::Vector2f(640,460),"Enorme"));
            break;
    }
    menu.mkMenu(el,default1,default2);
    // on donne le state
    MenuScreen::state = state;
    MenuScreen::frame = 0;
}
