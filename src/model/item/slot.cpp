#include "slot.h"

Slot::Slot()
{
    item=new Item(ItemType::none);
    amount=0;
}

Item* Slot::getItem(){
    return item;
}
int Slot::getAmount()
{
    return amount;
}

 bool Slot::canAccept(ItemType type,int amount)
{
    if((this->item->getType() == ItemType::none || this->item->getType() == type ) &&
            (amount + this->amount) <= item->getStackSize())
        return true;
    else
        return false;

}
bool Slot::isEmpty()const
{
    if(amount== 0 || item->getType()==ItemType::none)
        return true;
    else
        return false;
}
void Slot::add(Item *item,int amount){
     if(isEmpty()){
        delete this->item;
        this->item = item;
     }
     if(this->item->getType()==item->getType())
           this->amount += amount;
}
int Slot::getRoomLeft()
{
    return item->getStackSize()-amount;
}

Slot::~Slot()
{
    //delete item; // fait crash
    //item = nullptr;
    //dtor
}
