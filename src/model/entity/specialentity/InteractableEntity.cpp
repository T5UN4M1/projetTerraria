#include "InteractableEntity.h"

InteractableEntity::InteractableEntity(SpecialEntityType type)
    :SpecialEntity(type)
{}

bool InteractableEntity::isMultiBlock(){
    return true;
}

bool InteractableEntity::isInteractable(){
    return true;
}

InteractableEntity::~InteractableEntity()
{}
