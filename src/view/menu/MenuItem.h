#ifndef MENUITEM_H
#define MENUITEM_H

#include <SFML/graphics.hpp>

class MenuItem
{
    public:
        MenuItem();

        void setHitbox(sf::FloatRect& hitbox);
        const sf::FloatRect& getHitbox()const;

        virtual void yield(bool selected)=0;
        virtual void initHitbox()=0;
    protected:
        sf::FloatRect hitbox;
    private:
};

#endif // MENUITEM_H
