#ifndef LOADINGSCREEN_H
#define LOADINGSCREEN_H


class LoadingScreen
{
    public:
        static int progression;//max 100000
        static int gameProgression;
        static unsigned frame;

        static void yield();

        static float getProgression();
        static void addProgression(int prog);
};

#endif // LOADINGSCREEN_H
