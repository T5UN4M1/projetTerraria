#ifndef ENTITY_H
#define ENTITY_H


class Entity
{
public:
    /**
    *Constructeur d'une entit� en g�neral selon le type d'entit�
    *\param x position x de l'entit�
    *\param y position y de l'entit�
    */
    Entity(float x,float y);
    /**
    * accesseur en lecture de la position x
    * \return la position x
    */
    inline float getX()const;
    /**
    * accesseur en lecture de la position y
    * \return la position y
    */
    inline float getY()const;
    /**
    * accesseur en �criture de la position x
    * \param x le nouveau x
    */
    inline void setX(float x);
    /**
    * accesseur en �criture de la position y
    * \param y le nouveau y
    */
    inline void setY(float y);
    /**
    * accesseur en �criture de la position (x,y)
    * \param x le nouveau x
    * \param y le nouveau y
    */
    inline void setPos(float x,float y);
    /**
    * deplace l'entit� avec la position donn�e en param�tre
    * \param x le nouveau x
    * \param y le nouveau y
    */
    inline void movePos(float x,float y);
    /**
    * d�place l'entit� donn�e en param�tre
    * \param entity l'entit� a d�placer
    */
    inline void movePos(const Entity& entity);
protected:
    float x;
    float y;

private:
};
float Entity::getX()const
{
    return x;
}
float Entity::getY()const
{
    return y;
}
void Entity::setX(float x)
{
    this->x = x;
}
void Entity::setY(float y)
{
    this->y = y;
}
void Entity::setPos(float x,float y)
{
    this->x = x;
    this->y = y;
}
void Entity::movePos(float x,float y)
{
    this->x += x;
    this->y += y;
}
void Entity::movePos(const Entity& entity)
{
    this->x += entity.x;
    this->y += entity.y;
}
#endif // ENTITY_H
