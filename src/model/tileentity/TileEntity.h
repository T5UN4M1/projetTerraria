#ifndef TILEENTITY_H
#define TILEENTITY_H

#include "WorldElements.h"
#include "HitboxType.h"

class TileEntity
{
    public:
        TileEntity(WorldElements we,HitboxType ht);

        WorldElements getType()const;
        inline HitboxType getHitbox()const;

    protected:
        WorldElements name;
        HitboxType type;

    private:

};


HitboxType TileEntity::getHitbox()const{
    return type;
}





#endif // TILEENTITY_H
