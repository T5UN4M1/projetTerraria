#ifndef __MULTIBLOCKENTITY__H__
#define __MULTIBLOCKENTITY__H__
#include "SpecialEntity.h"

class MultiBlockEntity: public SpecialEntity
{

public:

    MultiBlockEntity(SpecialEntityType type);

    virtual bool isMultiBlock();

    virtual bool isInteractable();

    virtual ~MultiBlockEntity();

    //OnRightClick()
    //OnLeftClick();

};

#endif
