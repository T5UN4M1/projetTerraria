#include "tool.h"

Tool::Tool()
{
    //ctor
}
Tool::Tool(ItemType type)
{
    this->type = type;
    switch(type)
    {
    case ItemType::copperPickaxe :
        this->isPickaxe=true;
        this->isAxe=false;
        this->isHammer=false;
        this->autoSwing=true;
        this->minDamage=2;
        maxDamage=3;
        useTime=20;
        ressourceDamage=5;
        criticalChance=5;
        criticalMultiplier=150;
        knockback=3;
        range=50;
              break;
    case ItemType::copperAxe:
        this->isPickaxe=false;
        this->isAxe=true;
        this->isHammer=false;
        this->autoSwing=true;
        this->minDamage=2;
        maxDamage=3;
        useTime=20;
        ressourceDamage=5;
        criticalChance=5;
        criticalMultiplier=150;
        knockback=5;
        range=50;
        break;
    case ItemType::copperHammer:
        this->isPickaxe=false;
        this->isAxe=false;
        this->isHammer=false;
        this->autoSwing=true;
        this->minDamage=1;
        maxDamage=2;
        useTime=20;
        ressourceDamage=5;
        criticalChance=5;
        criticalMultiplier=150;
        knockback=10;
        range=50;
        break;
    case ItemType::copperSword:
        this->isPickaxe=false;
        this->isAxe=false;
        this->isHammer=false;
        this->autoSwing=false;
        //verifier les valeurs a partir d'ici
        this->minDamage=3;
        maxDamage=5;
        useTime=5;
        ressourceDamage=5;
        criticalChance=50;
        criticalMultiplier=5;
        knockback=3;
        range=50;
        break;
    default:
        this->isPickaxe=false;
        this->isAxe=false;
        this->isHammer=false;
        this->autoSwing=false;
        this->minDamage=0;
        maxDamage=0;
        useTime=0;
        ressourceDamage=0;
        criticalChance=0;
        criticalMultiplier=0;
        knockback=0;
        range=0;
        break;
    }
}
vector<int> Tool::getStats()
{

    vector <int> info(14);

    info[0]=getIdFromType(type);
    info[1]=getStackSize();
    if(isPickaxe==false)
        info[2]=0;
    else
        info[2]=1;
    if(isAxe==false)
        info[3]=0;
    else
        info[3]=1;
    if(isHammer==false)
        info[4]=0;
    else
        info[4]=1;
    if(autoSwing==false)
        info[5]=0;
    else
        info[5]=1;
    info[6]=minDamage;
    info[7]=maxDamage;
    info[8]=useTime;
    info[9]=ressourceDamage;
    info[10]=criticalChance;
    info[11]=criticalMultiplier;
    info[12]=knockback;
    info[13]=range;

    return info;
}


Tool::~Tool()
{
    //dtor
}
