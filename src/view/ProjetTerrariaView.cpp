#include "ProjetTerrariaView.h"
#include "Media.h"
#include "LoadingScreen.h"
#include "MenuScreen.h"
#include "Game.h"

int ProjetTerrariaView::frame = 0;
ViewStates ProjetTerrariaView::state = ViewStates::LAUNCHED;
std::unique_ptr<ProjetTerrariaCore> ProjetTerrariaView::game;

sf::RenderTexture ProjetTerrariaView::rt;
std::unique_ptr<sf::RenderWindow> ProjetTerrariaView::window;


void ProjetTerrariaView::run(){

    window.reset(new sf::RenderWindow(sf::VideoMode::getDesktopMode(),"projetTerraria",sf::Style::None));
    sf::Sprite sp = sf::Sprite();
    /*
    int renderWidth = 1920;
    int renderHeight = 1080;

    sf::VideoMode vm = sf::VideoMode::getDesktopMode();

    int screenWidth = vm.width;
    int screenHeight = vm.height;

    if(screenWidth != renderWidth || screenHeight != renderHeight){
        float renderRescaleX = 1;
        float renderRescaleY = 1;
        renderRescaleX = screenWidth / renderWidth;
        renderRescaleY = screenHeight / renderHeight;
        sp.setScale(0.9,0.9);//renderRescaleX,renderRescaleY);
    }*/

    window->setVerticalSyncEnabled(true);

    rt.create(1920,1080);

    while (window->isOpen()){
        sf::Event event;
        while (window->pollEvent(event)){
            if(event.type == sf::Event::Closed)
                window->close();
        }
        window->clear();
        rt.clear();
        yield();

        rt.display();
        sp.setTexture(rt.getTexture());
        window->draw(sp);
        window->display();


    }
}
void ProjetTerrariaView::yield(){
    /*enum states {
        LAUNCHED = 0,
        LOADING_MINIMAL = 1,
        LOADING_FOR_MENU = 2,
        MENU = 3
    };*/

    if(Media::loading){
        LoadingScreen::yield();
    }
    sf::Text t;
    t.setFont(Media::font[0]);
    t.setColor(sf::Color::Red);
    switch(state){
        case ViewStates::LAUNCHED:
            setState(ViewStates::LOADING_MINIMAL);
            break;
        case ViewStates::LOADING_MINIMAL :
            if(!Media::loading){

                setState(ViewStates::LOADING_FOR_MENU);
            }
            break;
        case ViewStates::LOADING_FOR_MENU:
            if(!Media::loading){
                setState(ViewStates::MENU);
            }
            break;
        case ViewStates::MENU:
            MenuScreen::yield();
            break;
        case ViewStates::LOADING_FOR_GAME:
            if(!Media::loading){
                setState(ViewStates::GAME);
            }
            break;
        case ViewStates::GAME:
            Game::yield();
            break;
    }


    ++frame;
}
void ProjetTerrariaView::setState(ViewStates state){
    switch(state){
    case ViewStates::LOADING_MINIMAL :
        Media::load(&Media::loadMinimal);
        break;
    case ViewStates::LOADING_FOR_MENU :
        Media::load(&Media::loadForMenu);
        break;
    case ViewStates::LOADING_FOR_GAME :
        Media::load(&Media::loadForGame);
        break;
    }
    ProjetTerrariaView::state = state;
    ProjetTerrariaView::frame = 0;
    //yield();
}
