#ifndef GAME_H
#define GAME_H


#include "../model/util/Vector2D.h"
#include "../model/tileentity/TileEntity.h"
#include "../model/ProjetTerrariaCore.h"
#include "../model/util/Vector2D.h"
#include "../model/tileentity/WorldElements.h"
#include "game/Chunk.h"
#include <SFML/graphics.hpp>

class Game
{
    public:
        static int mapSize;
        static int frame;

        static int chunkSize;

        static Vector2D<Chunk> chunks;
        static ProjetTerrariaCore* game;

        static void yield();

        static void setupGamePrototype(int xSize,int ySize);

        static float translatePosition(float modelPos,float screenPos);

};

#endif // GAME_H
