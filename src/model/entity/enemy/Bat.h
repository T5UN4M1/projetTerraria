#ifndef BAT_H
#define BAT_H

#include "../Enemy.h"
#include "EnemyType.h"

class Bat : public Enemy
{
    public:
        Bat(float x,float y);
        virtual void yield();
    protected:


    private:
};

#endif // BAT_H
