#include "Media.h"
#include "LoadingScreen.h"

std::unordered_map<std::string,sf::Texture> Media::txt;
std::unordered_map<std::string,sf::Sprite> Media::itemIcons;
std::unordered_map<std::string,sf::Sprite> Media::tools;
std::unordered_map<std::string,sf::Sprite> Media::other;
bool Media::loading = false;

sf::Sprite Media::spLoadingScreenBg;
sf::Sprite Media::spLoadingScreenBar;
sf::Sprite Media::spLoadingScreenGauge;

sf::Sprite Media::spMenuBg;

std::vector<sf::Sprite> Media::spPlayer;

std::vector<sf::Font> Media::font;

std::unique_ptr<sf::Thread> Media::t;


void Media::loadMinimal(){
    txt["loadingBg"].loadFromFile("res/loadScreen/loadingBg.jpg");
    txt["loadingBar"].loadFromFile("res/loadScreen/loadingBar.png");
    txt["loadingGauge"].loadFromFile("res/loadScreen/loadingGauge.png");

    spLoadingScreenBg.setTexture(txt["loadingBg"]);
    spLoadingScreenGauge.setTexture(txt["loadingGauge"]);
    spLoadingScreenBar.setTexture(txt["loadingBar"]);

    loading = false;
}
void Media::loadForMenu(){
    for(unsigned i=0;i<4;++i){
        font.push_back(sf::Font());
    }
    font[0].loadFromFile("res/font/Arial.ttf");
    LoadingScreen::addProgression(10000);
    font[1].loadFromFile("res/font/Gunship.ttf");
    LoadingScreen::addProgression(10000);
    font[2].loadFromFile("res/font/OldLondon.ttf");
    LoadingScreen::addProgression(10000);
    font[3].loadFromFile("res/font/SomethingStrange.ttf");
    LoadingScreen::addProgression(10000);
    txt["menuBg"].loadFromFile("res/menu/menuBg.jpg");
    LoadingScreen::addProgression(60000);
    spMenuBg.setTexture(txt["menuBg"]);

    loading = false;
}
void Media::loadForGame(){
    txt["player"].loadFromFile("res/game/player.png");
    for(int i=0;i<6;++i){
        spPlayer.push_back(sf::Sprite(txt["player"]));
        spPlayer[i].setOrigin(17/2.0,23/2.0);
    }
    spPlayer[0].setTextureRect(sf::IntRect(16,7,17,23));
    spPlayer[1].setTextureRect(sf::IntRect(48,7,17,23));
    spPlayer[2].setTextureRect(sf::IntRect(80,7,17,23));
    spPlayer[3].setTextureRect(sf::IntRect(16,7,17,23));
    spPlayer[4].setTextureRect(sf::IntRect(48,7,17,23));
    spPlayer[5].setTextureRect(sf::IntRect(80,7,17,23));
    for(int i=3;i<6;++i){
        spPlayer[i].setScale(-1,1);
    }

    txt["tiles"].loadFromFile("res/game/tiles.png");

    txt["itemIronOre"].loadFromFile("res/game/items/Item_11.png");
    itemIcons["ironOre"].setTexture(txt["itemIronOre"]);
    txt["itemDoor"].loadFromFile("res/game/items/Item_25.png");
    itemIcons["door"].setTexture(txt["itemDoor"]);
    txt["itemArrow"].loadFromFile("res/game/items/Item_40.png");
    itemIcons["arrow"].setTexture(txt["itemArrow"]);
    txt["itemBullet"].loadFromFile("res/game/items/Item_97.png");
    itemIcons["bullet"].setTexture(txt["itemBullet"]);
    txt["itemCoal"].loadFromFile("res/game/items/Item_192.png");
    itemIcons["coal"].setTexture(txt["itemCoal"]);

    txt["itemSDMG"].loadFromFile("res/game/items/Item_1553.png");
    itemIcons["SDMG"].setTexture(txt["itemSDMG"]);
    itemIcons["SDMG"].setScale(32.0/66.0,32.0/32.0);
    tools["SDMG"].setTexture(txt["itemSDMG"]);

    txt["itemTsunami"].loadFromFile("res/game/items/Item_2624.png");
    itemIcons["tsunami"].setTexture(txt["itemTsunami"]);
    itemIcons["tsunami"].setScale(32.0/28.0,32.0/58.0);
    tools["tsunami"].setTexture(txt["itemTsunami"]);

    txt["itemBow"].loadFromFile("res/game/items/Item_3504.png");
    itemIcons["bow"].setTexture(txt["itemBow"]);
    itemIcons["bow"].setScale(32.0/16.0,32.0/32.0);
    tools["bow"].setTexture(txt["itemBow"]);

    txt["itemHammer"].loadFromFile("res/game/items/Item_3505.png");
    itemIcons["hammer"].setTexture(txt["itemHammer"]);
    itemIcons["hammer"].setScale(32.0/32.0,32.0/32.0);
    tools["hammer"].setTexture(txt["itemHammer"]);

    txt["itemAxe"].loadFromFile("res/game/items/Item_3506.png");
    itemIcons["axe"].setTexture(txt["itemAxe"]);
    itemIcons["axe"].setScale(32.0/32.0,32.0/28.0);
    tools["axe"].setTexture(txt["itemAxe"]);

    txt["itemSword"].loadFromFile("res/game/items/Item_3508.png");
    itemIcons["sword"].setTexture(txt["itemSword"]);
    itemIcons["sword"].setScale(32.0/32.0,32.0/32.0);
    tools["sword"].setTexture(txt["itemSword"]);

    txt["itemPickaxe"].loadFromFile("res/game/items/Item_3509.png");
    itemIcons["pickaxe"].setTexture(txt["itemPickaxe"]);
    itemIcons["pickaxe"].setScale(32.0/32.0,32.0/32.0);
    tools["pickaxe"].setTexture(txt["itemPickaxe"]);

    txt["itemPhantasm"].loadFromFile("res/game/items/Item_3540.png");
    itemIcons["phantasm"].setTexture(txt["itemPhantasm"]);
    itemIcons["phantasm"].setScale(32.0/32.0,32.0/54.0);
    tools["phantasm"].setTexture(txt["itemPhantasm"]);

    txt["itemPhantasmTsunami"].loadFromFile("res/game/items/itemmm.png");
    itemIcons["phantasmTsunami"].setTexture(txt["itemPhantasmTsunami"]);
    itemIcons["phantasmTsunami"].setScale(32.0/28.0,32.0/58.0);
    tools["phantasmTsunami"].setTexture(txt["itemPhantasmTsunami"]);

    txt["enemyBat"].loadFromFile("res/game/enemy.png");
    other["enemyBat"].setTexture(txt["enemyBat"]);

    txt["inventorySlot"].loadFromFile("res/game/Inventory.png");
    other["inventorySlot"].setTexture(txt["inventorySlot"]);

    txt["inventorySlot2"].loadFromFile("res/game/Inventory2.png");
    other["inventorySlot2"].setTexture(txt["inventorySlot2"]);
    //ProjetTerrariaCore(3000,1000,&LoadingScreen::progression);
    loading = false;
}
void Media::load(void (*f)()){
    LoadingScreen::progression = 0;
    loading = true;

    t.reset(new sf::Thread(f));
    t->launch();

    //f();
}
