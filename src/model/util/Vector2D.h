#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <vector>
#include <memory>

template <typename T>
class Vector2D
{
public:
    /**
    *Permet de construire un vecteur 2D
    *@param x nombre de ligne
    *@param y nombre de colonnes
    *@param def le type d'elements
    */
    Vector2D(int x,int y,T def)
    {
        reset(x,y,def);
    }
    /**
    *Permet de construire un vecteur 2D
    *@param x nombre de ligne
    *@param y nombre de colonnes
    */
    Vector2D(int x,int y)
    {
        reset(x,y);
    }
    /**
    *Permet de construire un vecteur 2D avec 0 ligne et colonne
    */
    Vector2D()
    {

        reset(0,0);
    }
    /**
    *Permet de changer la taille du vecteur 2D et le type d'�lement qu'il contiens
    *@param x nombre de ligne
    *@param y nombre de colonnes
    *@param def le type d'elements
    */
    void reset(int x,int y,T def)
    {
        xSize = x;
        ySize = y;
        data.resize(x*y);
        for(int xx=0; xx<x; ++xx)
        {
            for(int yy=0; yy<y; ++yy)
            {
                data[getId(xx,yy)] = def;
            }
        }
    }
    /**
    *Permet de changer la taille du vecteur 2D
    *@param x nombre de ligne
    *@param y nombre de colonnes
    */
    void reset(int x,int y)
    {
        xSize = x;
        ySize = y;
        data.resize(x*y);
    }
    /**
    * Accesseur en lecture d'un �lement du vecteur a partir de sa position
    *@param x num�ro de ligne ligne
    *@param y num�ro de colonnes
    *@return un �lement de type T
    */
    const T& get(unsigned x,unsigned y)const
    {
        return data[getId(x,y)];
    }
    /**
    *Accesseur en �criture d'un �lement du vecteur a partir de sa position
    *@param x num�ro de ligne ligne
    *@param y num�ro de colonnes
    *@param val l'�lement en question
    */
    void set(unsigned x,unsigned y,T val)
    {
        data[getId(x,y)] = val;
    }
    /**
    * Accesseur en �criture d'un �lement du vecteur a partir de sa position
    *@param x num�ro de ligne ligne
    *@param y num�ro de colonnes
    */
    T& set(unsigned x,unsigned y)
    {
        return data[getId(x,y)];
    }
    /**
    * Accesseur en lecture du nombre de ligne du vecteur 2D
    *@return nombre de ligne du vecteur 2D
    */
    unsigned getXSize()const
    {
        return xSize;
    }
    /**
    * Accesseur en lecture du nombre de colonne du vecteur 2D
    *@return nombre de colonne du vecteur 2D
    */
    unsigned getYSize()const
    {
        return ySize;
    }
    /**
    * Accesseur en lecture de l'id d'un �lement du vecteur a partir de sa position
    *@param x num�ro de ligne ligne
    *@param y num�ro de colonnes
    *@return l'id de l'�lement a la position x y
    */
    unsigned getId(unsigned x,unsigned y)const
    {
        return y*ySize + x;
    }


protected:
    std::vector<T> data;
    unsigned xSize;
    unsigned ySize;



private:
};

#endif // VECTOR2D_H
