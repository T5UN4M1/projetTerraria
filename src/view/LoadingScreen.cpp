#include "LoadingScreen.h"

#include "ProjetTerrariaView.h"
#include "Media.h"
int LoadingScreen::gameProgression = 0;
int LoadingScreen::progression = 0;
unsigned LoadingScreen::frame = 0;


void LoadingScreen::yield(){


    ProjetTerrariaView::rt.draw(Media::spLoadingScreenBg);


    // ajustement de la barre de chargement en fonction de la progression

    // target : 564 x 16
    float barWidth = 198;
    float barHeight = 6;

    float targetWidth = 564;
    float targetHeight = 16;

    float offsetX = 28;
    float offsetY = 40;

    float LoadingBarX = 1920/2 - 310;
    float LoadingBarY = 900;

    float scaleX = targetWidth / barWidth * getProgression();
    float scaleY = targetHeight / barHeight;

    Media::spLoadingScreenGauge.setTextureRect(sf::IntRect(0,((frame/2)%16)*6,barWidth,barHeight));
    Media::spLoadingScreenGauge.setScale(scaleX,scaleY);
    Media::spLoadingScreenGauge.setPosition(LoadingBarX+offsetX,LoadingBarY+offsetY);
    ProjetTerrariaView::rt.draw(Media::spLoadingScreenGauge);

    Media::spLoadingScreenBar.setPosition(LoadingBarX,LoadingBarY);
    ProjetTerrariaView::rt.draw(Media::spLoadingScreenBar);
    ++frame;
}
float LoadingScreen::getProgression(){
    return progression/100000.0;
}
void LoadingScreen::addProgression(int prog){
    progression += prog;
    if(progression > 100000){
        progression = 100000;
    }
}
