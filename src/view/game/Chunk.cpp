#include "Chunk.h"
#include "../../model/tileentity/WorldElements.h"

Chunk::Chunk(sf::Vector2i size,sf::Vector2i start):start(start),size(size){
    //tiles.reset(size,size);
    tiles.resize(size.x*size.y*4);
    tiles.setPrimitiveType(sf::Quads);
}
Chunk::Chunk(){
    tiles.resize(0);
}
void Chunk::updateVertex(int x,int y,sf::Vertex* v){
    v[0].texCoords = sf::Vector2f(x,y);
    v[1].texCoords = sf::Vector2f(x+16,y);
    v[2].texCoords = sf::Vector2f(x+16,y+16);
    v[3].texCoords = sf::Vector2f(x,y+16);
}
void Chunk::reload(Vector2D<std::unique_ptr<TileEntity>>& map){
    for(unsigned x=0;x<size.x;++x){
        for(unsigned y=0;y<size.y;++y){

            //tiles.get(start.x + x,start.y + y) = sf::VertexArray(sf::Quads , 4);

            sf::Vertex* v = &tiles[(x + y * size.x) * 4]; //tiles.get(start.x + x,start.y + y); // on r�cup�re le vertex � modifier

            v[0].position = sf::Vector2f(x*16, y*16);
            v[1].position = sf::Vector2f(x*16 + 16, y*16);
            v[2].position = sf::Vector2f(x*16 + 16, y*16 + 16);
            v[3].position = sf::Vector2f(x*16, y*16 + 16);
            switch(map.get(start.x + x,start.y + y)->getType()){
            case WorldElements::dirt :
                /*v[0].texCoords = sf::Vector2f(0,0);
                v[1].texCoords = sf::Vector2f(16,0);
                v[2].texCoords = sf::Vector2f(16,16);
                v[3].texCoords = sf::Vector2f(0,16);*/
                updateVertex(0,0,v);
                break;

            case WorldElements::stone :
                updateVertex(16,0,v);
                break;
            case WorldElements::woodPlatform :
                updateVertex(32,0,v);
                break;
            case WorldElements::furnace:
                updateVertex(16*3,0,v);
                break;
            case WorldElements::treeSapling:
                updateVertex(16*4,0,v);
                break;
            case WorldElements::torch:
                updateVertex(16*5,0,v);
                break;
            case WorldElements::treeTrunk:
                updateVertex(16*6,0,v);
                break;
            case WorldElements::ironOre:
                updateVertex(16*7,0,v);
                break;
            case WorldElements::door1:
                updateVertex(16*8,0*16,v);
                break;
            case WorldElements::door2:
                updateVertex(16*8,1*16,v);
                break;
            case WorldElements::door3:
                updateVertex(16*8,2*16,v);
                break;
            case WorldElements::door11:
                updateVertex(16*9,0*16,v);
                break;
            case WorldElements::door12:
                updateVertex(16*9,1*16,v);
                break;
            case WorldElements::door13:
                updateVertex(16*9,2*16,v);
                break;
            case WorldElements::door21:
                updateVertex(16*10,0*16,v);
                break;
            case WorldElements::door22:
                updateVertex(16*10,1*16,v);
                break;
            case WorldElements::door23:
                updateVertex(16*10,2*16,v);
                break;
            case WorldElements::coalOre:
                updateVertex(16*11,0*16,v);
                break;
            case WorldElements::branch1:
                updateVertex(16*12,0*16,v);
                break;
            case WorldElements::branch2:
                updateVertex(16*13,0*16,v);
                break;
            case WorldElements::woodPlank:
                updateVertex(16*14,0*16,v);
                break;
            case WorldElements::treeTop11:
                updateVertex(16*15,0*16,v);
                break;
            case WorldElements::treeTop12:
                updateVertex(16*15,1*16,v);
                break;
            case WorldElements::treeTop13:
                updateVertex(16*15,2*16,v);
                break;
            case WorldElements::treeTop14:
                updateVertex(16*15,3*16,v);
                break;
            case WorldElements::treeTop15:
                updateVertex(16*15,4*16,v);
                break;
            case WorldElements::treeTop21:
                updateVertex(16*16,0*16,v);
                break;
            case WorldElements::treeTop22:
                updateVertex(16*16,1*16,v);
                break;
            case WorldElements::treeTop23:
                updateVertex(16*16,2*16,v);
                break;
            case WorldElements::treeTop24:
                updateVertex(16*16,3*16,v);
                break;
            case WorldElements::treeTop25:
                updateVertex(16*16,4*16,v);
                break;
            case WorldElements::treeTop31:
                updateVertex(17*16,0*16,v);
                break;
            case WorldElements::treeTop32:
                updateVertex(17*16,1*16,v);
                break;
            case WorldElements::treeTop33:
                updateVertex(17*16,2*16,v);
                break;
            case WorldElements::treeTop34:
                updateVertex(17*16,3*16,v);
                break;
            case WorldElements::treeTop35:
                updateVertex(17*16,4*16,v);
                break;
            case WorldElements::treeTop41:
                updateVertex(18*16,0*16,v);
                break;
            case WorldElements::treeTop42:
                updateVertex(18*16,1*16,v);
                break;
            case WorldElements::treeTop43:
                updateVertex(18*16,2*16,v);
                break;
            case WorldElements::treeTop44:
                updateVertex(18*16,3*16,v);
                break;
            case WorldElements::treeTop45:
                updateVertex(18*16,4*16,v);
                break;
            case WorldElements::treeTop51:
                updateVertex(19*16,0*16,v);
                break;
            case WorldElements::treeTop52:
                updateVertex(19*16,1*16,v);
                break;
            case WorldElements::treeTop53:
                updateVertex(19*16,2*16,v);
                break;
            case WorldElements::treeTop54:
                updateVertex(19*16,3*16,v);
                break;
            case WorldElements::treeTop55:
                updateVertex(19*16,4*16,v);
                break;
            case WorldElements::nothing :
                v[0].color = sf::Color::Cyan;
                v[1].color = sf::Color::Cyan;
                v[2].color = sf::Color::Cyan;
                v[3].color = sf::Color::Cyan;
                break;
            }
        }
    }
}
