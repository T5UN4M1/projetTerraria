#ifndef VIEWSTATES_H
#define VIEWSTATES_H


enum class ViewStates
{
    LAUNCHED,
    LOADING_MINIMAL,
    LOADING_FOR_MENU,
    MENU,
    LOADING_FOR_GAME,
    GAME
};

#endif // VIEWSTATES_H
