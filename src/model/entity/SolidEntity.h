#ifndef SOLIDENTITY_H
#define SOLIDENTITY_H

#include "MobileEntity.h"
#include "../hitbox/RectangleHitbox.h"

#include "SolidEntityType.h"


class SolidEntity : public MobileEntity
{
    public:
        SolidEntity(float x,float y,float speed,float angle,RectangleHitbox hitbox,SolidEntityType type);
        inline float getMinX() const;
        inline float getMinY() const;
        inline float getMaxX() const;
        inline float getMaxY() const;
        inline float getCenterX() const;
        inline float getCenterY() const;

        inline float getWidth()const;
        inline float getHeight()const;

        inline bool collisionTop(SolidEntity e)const;
        inline bool collisionRight(SolidEntity e)const;
        inline bool collisionLeft(SolidEntity e)const;
        inline bool collisionBottom(SolidEntity e)const;

        inline bool collisionWith(SolidEntity se)const;

        inline int getHashedMinX(int caseSize)const;
        inline int getHashedMinY(int caseSize)const;
        inline int getHashedMaxX(int caseSize)const;
        inline int getHashedMaxY(int caseSize)const;

        int getHashedMinX()const;
        int getHashedMinY()const;
        int getHashedMaxX()const;
        int getHashedMaxY()const;

        bool isOnABlock()const;

        void sEntityMove();


    protected:
        RectangleHitbox hitbox;
        SolidEntityType type;

    private:
};

float SolidEntity::getMinX() const{
    return x;
}

float SolidEntity::getMinY() const{
    return y;
}

float SolidEntity::getMaxX() const{
    return x + hitbox.getWidth();
}

float SolidEntity::getMaxY() const{
    return y + hitbox.getHeight();
}

float SolidEntity::getCenterX() const{
    return x + hitbox.getWidth()/2;
}

float SolidEntity::getCenterY() const{
    return y + hitbox.getHeight()/2;
}

float SolidEntity::getWidth()const{
    return hitbox.getWidth();
}

float SolidEntity::getHeight()const{
    return hitbox.getHeight();
}

bool SolidEntity::collisionTop(SolidEntity e)const{
    return getMinY() > e.getMaxY();
}

bool SolidEntity::collisionRight(SolidEntity e)const{
    return getMaxX() < e.getMinX();
}

bool SolidEntity::collisionLeft(SolidEntity e)const{
    return getMinX() > e.getMaxX();
}

bool SolidEntity::collisionBottom(SolidEntity e)const{
    return getMaxY() < e.getMinY();
}

bool SolidEntity::collisionWith(SolidEntity se)const
{
    return !(collisionTop(se) || collisionRight(se) || collisionRight(se) || collisionLeft(se)   );
}

int SolidEntity::getHashedMinX(int caseSize)const{
    return static_cast<int>(getMinX() / caseSize);
}

int SolidEntity::getHashedMinY(int caseSize)const{
    return static_cast<int>(getMinY() / caseSize);
}

int SolidEntity::getHashedMaxX(int caseSize)const{
    return static_cast<int>(getMaxX() / caseSize);
}

int SolidEntity::getHashedMaxY(int caseSize)const{
    return static_cast<int>(getMaxY() / caseSize);
}

#endif // SOLIDENTITY_H
