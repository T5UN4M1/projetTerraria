#ifndef __TICKINGENTITY__H__
#define __TICKINGENTITY__H__
#include "SpecialEntity.h"

class TickingEntity: public SpecialEntity
{

protected:
    int x=0;
    int y=0;

public:

    TickingEntity(SpecialEntityType type, int a_a, int a_y);

    virtual bool isMultiBlock();

    virtual bool isInteractable();

    virtual ~TickingEntity();

    //yield()

};

#endif
