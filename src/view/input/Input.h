#ifndef INPUT_H
#define INPUT_H

#include <vector>
#include <memory>
#include "button.h"
#include "InputAction.h"

class Input
{
    public:
        Input();
        void reset();
        void yield();
        void addButton(InputAction* button);
        bool isPressed(int id)const;
        bool isActivated(int id, int delay=30, int interval=5)const;
    protected:
        std::vector<std::unique_ptr<InputAction>> keySet;
};

#endif // INPUT_H
