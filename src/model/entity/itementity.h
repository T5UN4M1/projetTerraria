#ifndef ITEMENTITY_H
#define ITEMENTITY_H
#include "model/entity/Entity.h"
#include "model/item/item.h"
#include "model/hitbox/RectangleHitbox.h"
#include "model/entity/SolidEntity.h"
#include <memory>
#include <string>
#include <iostream>


class ItemEntity :public Entity{
    public:
        /**
        * Constructeur d'une entit้ item
        * \param x la position x de l'item
        * \param y la position y de l'item
        * \param type le type de l'item
        */
        ItemEntity(float x,float y, ItemType type);
         /**
        * Constructeur d'une entit้ item
        * \param x la position x de l'item
        * \param y la position y de l'item
        * \param type le type de l'item
        * \param amount nombre d'occurence de cette item
        */
        ItemEntity(float x,float y,ItemType type,int amount);
        /**
        * destructeur de l'entit้ item
        */
        virtual ~ItemEntity();

    protected:
    //std::unique_ptr<Item> item;

    int amount;

    private:
};

#endif // ITEMENTITY_H
