#include "inventory.h"


Inventory::Inventory()
{

}
Inventory::Inventory(int x,int y)
{
    slots.reset(x,y);
}
void Inventory::add(int x,int y,Item *item,int amount)
{
    (slots.set(x,y)).add(item,amount);
}
void Inventory::add(Item *item, int amount)
{
    int i=0;
    int j=0;
    int dif;
    bool stop=false;
    while(i<slots.getXSize() && !stop)
    {

        while(j<slots.getYSize() && !stop)
        {
            if(slots.set(i,j).getItem()->getType()==item->getType())
            {

                if(amount + slots.set(i,j).getAmount()<=slots.set(i,j).getItem()->getStackSize())
                {
                    slots.set(i,j).add(item,amount);
                    stop=true;
                }
                else if(slots.set(i,j).getAmount()<slots.set(i,j).getItem()->getStackSize())
                {
                    dif=slots.set(i,j).getItem()->getStackSize()-slots.set(i,j).getAmount();
                    amount=amount-dif;
                    slots.set(i,j).add(item,dif);

                }
            }
            j++;
        }
        i++;
    }

}

int Inventory::getRoomLeft()
{
    int cpt=0;
    for(int i=0; i<slots.getXSize(); i++)
    {
        for(int j=0; slots.getYSize(); j++)
        {
            if(slots.set(i,j).getItem()->getType()==ItemType::none || slots.set(i,j).getAmount()==0)
                cpt++;
        }
    }
    return cpt;
}
Inventory::~Inventory()
{
    //dtor
}
